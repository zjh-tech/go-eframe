package eframe

import (
	"os"
	"os/signal"
	"syscall"
)

type ISignalQuitDealer interface {
	Quit()
}

type ISignalOnQuitDealer interface {
	OnQuit()
}

type SignalDealer struct {
	signalChan   chan os.Signal
	quitDealer   ISignalQuitDealer
	onQuitDealer ISignalOnQuitDealer
}

func NewSignalDealer() *SignalDealer {
	return &SignalDealer{}
}

func (s *SignalDealer) SetQuitDealer(quitDealer ISignalQuitDealer) {
	s.quitDealer = quitDealer
}

func (s *SignalDealer) SetOnQuitDealer(onQuitDealer ISignalOnQuitDealer) {
	s.onQuitDealer = onQuitDealer
}

// 自定义信号  USER1:CPU   USER2:MEMORY
func (s *SignalDealer) StartSignalGoroutine() {
	s.signalChan = make(chan os.Signal, 1)
	signal.Notify(s.signalChan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	ELog.Info("Start Signal Goroutine")

	signal, ok := <-s.signalChan
	if !ok {
		return
	}
	switch signal {
	//kill 2 kill 15 进程可以收尾工作
	case syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT:
		ELog.Info("Stop Signal Goroutine")
		if s.onQuitDealer != nil {
			s.onQuitDealer.OnQuit()
		} else if s.quitDealer != nil {
			s.quitDealer.Quit()
		}
		return
	}
}

var GSignalDealer *SignalDealer

func init() {
	GSignalDealer = NewSignalDealer()
}
