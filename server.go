package eframe

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	"gitee.com/zjh-tech/go-elog"
	"gitee.com/zjh-tech/go-enet"
	"gitee.com/zjh-tech/go-etimer"
	"github.com/go-redis/redis"
)

type IServer interface {
	Init() bool

	Run()

	Quit()

	UnInit()
}

type ZeroLogicFuncType func()

type Server struct {
	configPath string
	terminate  bool
	logger     *elog.Logger
	serverType uint32
	ip         string

	lbState       uint32
	lbInitFlag    bool
	lbFlag        bool
	timerRegister *etimer.TimerRegister
	lbSendChan    chan struct{}

	logPrefix string

	mainCtx           context.Context
	mainCtxWithCancel context.Context
	mainCancelFunc    context.CancelFunc
	wg                sync.WaitGroup
	updateQueue       chan struct{}

	Version string

	RedisCmd redis.Cmdable

	tomorrowZeroTime time.Time
	zeroLogicFunc    ZeroLogicFuncType
}

func (s *Server) IsQuit() bool {
	return s.terminate
}

func (s *Server) Quit() {
	if s.terminate {
		return
	}

	s.terminate = true

	if ELog != nil {
		ELog.Info("Server Quit")
	}

	if GSignalDealer.onQuitDealer != nil {
		time.Sleep(30 * time.Second) //30秒处理事件
	} else {
		time.Sleep(5 * time.Second)
	}

	if s.mainCancelFunc != nil {
		s.mainCancelFunc()
		s.wg.Wait()
	}
}

func (s *Server) GetServerId() uint64 {
	return GServerCfg.ServerInfo.ServerId
}

func (s *Server) GetToken() string {
	return GServerCfg.ServerInfo.Token
}

func (s *Server) GetServerType() uint32 {
	return s.serverType
}

func (s *Server) GetServerTypeStr() string {
	return GAppConfig.ServerType
}

func (s *Server) GetEtcdPrefix() string {
	return GAppConfig.EtcdPrefix
}

func (s *Server) GetIp() string {
	return s.ip
}

func (s *Server) GetLogger() *elog.Logger {
	return s.logger
}

func (s *Server) GetConfigPath() string {
	return s.configPath
}

func (s *Server) GetExtraConfigPath() string {
	return GServerCfg.ServerInfo.ExtraConfigPath
}

func (s *Server) GetLbState() uint32 {
	return s.lbState
}

func (s *Server) SetLbState(lbState uint32) {
	oldLbState := s.lbState
	s.lbState = lbState
	if s.lbFlag && s.lbState != oldLbState {
		s.lbSendChan <- struct{}{}
	}
}

func (s *Server) SetVersion(version string) {
	s.Version = version
	ELog.Infof("Server Version=%v", s.Version)
}

func (s *Server) IsDebugMode() bool {
	return len(os.Args) >= 2
}

func (s *Server) GetTimerRegister() *etimer.TimerRegister {
	return s.timerRegister
}

func (s *Server) InitApp(logPrefix string) error {
	s.InitAppLogic(logPrefix)

	//app_cfg.yaml
	appCfgPath := s.GetConfigPath() + "/app_cfg.yaml"
	if appCfg, readErr := ReadAppConfig(appCfgPath); readErr != nil {
		return fmt.Errorf("ReadServerCfg Error=%v", readErr)
	} else {
		GAppConfig = appCfg
	}

	if GAppConfig.ServerId == 0 {
		return fmt.Errorf("Server ServerID = 0")
	}

	if err := s.InitConfigCenter(); err != nil {
		return err
	}

	engineLogCfgKey := "public/enginelog.yaml"
	engineLogCfgValue, engineLogCfgErr := GConfigCenterClient.GetConfigDatas(engineLogCfgKey)
	if engineLogCfgErr != nil {
		return engineLogCfgErr
	}

	engineLogCfg, readEngineLogCfgErr := ReadEngineLogCfg(engineLogCfgValue)
	if readEngineLogCfgErr != nil {
		return readEngineLogCfgErr
	}
	GEngineLogCfg = engineLogCfg

	return nil
}

func (s *Server) InitServer(serverType uint32, initLbFlag bool, lbFlag bool) error {
	if GAppConfig.ServerId != GServerCfg.ServerInfo.ServerId {
		return fmt.Errorf("Server GAppConfig And GServerCfg Server Not Same")
	}

	if err := s.InitServerLogic(); err != nil {
		return err
	}

	s.lbInitFlag = initLbFlag
	s.lbFlag = lbFlag
	s.serverType = serverType

	if s.GetServerType() == 0 {
		return fmt.Errorf("Server ServerType = 0 Error")
	}

	ELog.Infof("%v:%v InitServer", strings.ToUpper(s.GetServerTypeStr()[:1])+s.GetServerTypeStr()[1:], s.GetServerId())

	if err := s.InitModuleByServerCfg(); err != nil {
		return err
	}

	if err := s.InitServiceDiscovery(); err != nil {
		return err
	}

	if err := s.InitServerLb(); err != nil {
		return err
	}

	return nil
}

func (s *Server) UnInit() {
	if enet.GNet != nil {
		enet.GNet.UnInit()
		enet.GNet = nil
	}

	if etimer.GTimerMgr != nil {
		etimer.GTimerMgr.UnInit()
		etimer.GTimerMgr = nil
	}

	if s.logger != nil {
		s.logger.UnInit()
		s.logger = nil
	}
}

func (s *Server) GetMainCtx() context.Context {
	return s.mainCtx
}

func (s *Server) GetMainCtxWithCancel() context.Context {
	return s.mainCtxWithCancel
}

func (s *Server) StartTimerGoroutine(d time.Duration) {
	s.updateQueue = make(chan struct{}, 10)
	s.wg.Add(1)

	go func() {
		defer func() {
			s.wg.Done()
		}()

		runTimer := time.NewTimer(d)
		defer func() {
			runTimer.Stop()
		}()

		for {
			select {
			case <-runTimer.C:
				{
					if len(s.updateQueue) == 0 {
						s.updateQueue <- struct{}{}
					}
					runTimer.Reset(d)
				}
			case <-s.mainCtxWithCancel.Done():
				return
			}
		}
	}()
}

func (s *Server) GetUpdateChannel() chan struct{} {
	return s.updateQueue
}

func (s *Server) LbNotify() {
	lbReq := &LbRegisterReq{}
	lbReq.LbInfo = &LbSpec{
		ServiceID:   s.GetServerId(),
		EtcdPrefix:  s.GetEtcdPrefix(),
		ServiceType: s.GetServerType(),
		State:       s.GetLbState(),
	}
	//负载:服务器之间使用的ServerID,对外使用的OuterAddr
	if GServerCfg.LbOuterAddr != "" {
		lbReq.LbInfo.Addr = GServerCfg.LbOuterAddr
	}

	if s.lbInitFlag {
		GLBClient.SendLbRegisterReq(lbReq)

		if len(lbReq.LbInfo.Addr) != 0 {
			ELog.Debugf("LbNotify: Id=%v Type=%v-%v,State=%v Addr=%v", lbReq.LbInfo.ServiceID, lbReq.LbInfo.ServiceType, s.GetServerTypeStr(), lbReq.LbInfo.State, lbReq.LbInfo.Addr)
		} else {
			ELog.Debugf("LbNotify: Id=%v Type=%v-%v State=%v", lbReq.LbInfo.ServiceID, lbReq.LbInfo.ServiceType, s.GetServerTypeStr(), lbReq.LbInfo.State)
		}
	}
}

func (s *Server) StartLbGoroutine() {
	s.wg.Add(1)
	defer func() {
		s.wg.Done()
	}()

	go func() {
		updTimer := time.NewTicker(60 * time.Second)
		defer updTimer.Stop()

		for {
			select {
			case <-updTimer.C:
				{
					s.LbNotify()
				}
			case <-s.lbSendChan:
				{
					s.LbNotify()
				}
			case <-s.mainCtxWithCancel.Done():
				return
			}
		}
	}()
}

func AddServiceCbFunc(key string, val string) {
	etcdServiceInfo := &EctdServiceSpec{}
	err := json.Unmarshal([]byte(val), etcdServiceInfo)
	if err != nil {
		ELog.Errorf("Server AddServiceCbFunc Key=%v Val=%v Json.Unmarshal Error=%v", key, val, err)
		return
	}

	if etcdServiceInfo.ServerId == GServer.GetServerId() {
		return
	}

	verify := enet.VerifySessionSpec{
		ServerID:      GServerCfg.ServerInfo.ServerId,
		ServerType:    GServer.GetServerType(),
		ServerTypeStr: GServerCfg.ServerInfo.ServerTypeStr,
		Token:         etcdServiceInfo.Token,
	}
	verify.Addr = GServer.GetIp()

	remote := enet.RemoteSessionSpec{
		ServerID:      etcdServiceInfo.ServerId,
		ServerType:    etcdServiceInfo.ServerType,
		ServerTypeStr: etcdServiceInfo.ServerTypeStr,
	}

	if len(etcdServiceInfo.Outer) != 0 {
		remote.Addr = etcdServiceInfo.Outer
	} else if len(etcdServiceInfo.Inner) != 0 {
		remote.Addr = etcdServiceInfo.Inner
	} else {
		ELog.Errorf("Server AddServiceCbFunc  EtcdServiceInfo=%v Remote Addr Error", etcdServiceInfo)
		return
	}

	if enet.GSSSessionMgr.IsInConnectCache(remote.ServerID) {
		ELog.Errorf("Server AddServiceCbFunc IsInConnectCache EtcdServiceInfo=%v", etcdServiceInfo)
		return
	}

	if enet.GSSSessionMgr.FindSessionByServerId(remote.ServerID) != nil {
		ELog.Errorf("Server AddServiceCbFunc Exist ServerId SSSession EtcdServiceInfo=%v", etcdServiceInfo)
		return
	}
	enet.GSSSessionMgr.SSServerConnect(verify, remote, enet.BigSendChannelSize)
}

func DelServiceCbFunc(key string, val string) {
	etcdServiceInfo := &EctdServiceSpec{}
	err := json.Unmarshal([]byte(val), etcdServiceInfo)
	if err != nil {
		ELog.Errorf("Server DelServiceCbFunc  Key=%v Val=%v  Json.Unmarshal Error=%v", key, val, err)
		return
	}
	elog.Warnf("%v:%v:%v DelServiceCbFunc Error", etcdServiceInfo.ServerId, etcdServiceInfo.ServerTypeStr, etcdServiceInfo.ServerType)

	// sess := enet.GSSSessionMgr.FindSessionByServerId(etcdServiceInfo.ServerId)
	// if sess == nil {
	// 	ELog.Warnf("Server DelServiceCbFunc FindSessionByServerId Is Nil", etcdServiceInfo.ServerId)
	// 	return
	// }
	// sssess := sess.(*enet.SSSession)
	// if sssess == nil {
	// 	ELog.Errorf("Server DelServiceCbFunc SSSession Is Nil EtcdServiceInfo=%v", etcdServiceInfo)
	// 	return
	// }

	// sssess.Terminate()
}

func UpdServiceCbFunc(vals []string) {
	if vals == nil {
		return
	}

	for _, val := range vals {
		etcdServiceInfo := &EctdServiceSpec{}
		err := json.Unmarshal([]byte(val), etcdServiceInfo)
		if err != nil {
			ELog.Errorf("Server UpdServiceCbFunc Val=%v  Json.Unmarshal Error=%v", val, err)
			continue
		}

		if etcdServiceInfo.ServerId == GServer.GetServerId() {
			continue
		}

		if enet.GSSSessionMgr.IsInConnectCache(etcdServiceInfo.ServerId) {
			ELog.Debugf("Server UpdServiceCbFunc IsInConnectCache EtcdServiceInfo=%v", etcdServiceInfo)
			return
		}

		if enet.GSSSessionMgr.FindSessionByServerId(etcdServiceInfo.ServerId) != nil {
			ELog.Debugf("Server UpdServiceCbFunc Exist ServerId SSSession EtcdServiceInfo=%v", etcdServiceInfo)
			return
		}

		verify := enet.VerifySessionSpec{
			ServerID:      GServerCfg.ServerInfo.ServerId,
			ServerType:    GServer.GetServerType(),
			ServerTypeStr: GServerCfg.ServerInfo.ServerTypeStr,
			Token:         etcdServiceInfo.Token,
		}

		verify.Addr = GServer.GetIp()

		remote := enet.RemoteSessionSpec{
			ServerID:      etcdServiceInfo.ServerId,
			ServerType:    etcdServiceInfo.ServerType,
			ServerTypeStr: etcdServiceInfo.ServerTypeStr,
		}
		if len(etcdServiceInfo.Outer) != 0 {
			remote.Addr = etcdServiceInfo.Outer
		} else if len(etcdServiceInfo.Inner) != 0 {
			remote.Addr = etcdServiceInfo.Inner
		} else {
			ELog.Errorf("Server UpdServiceCbFunc  EtcdServiceInfo=%v Remote Addr Error", etcdServiceInfo)
			return
		}
		enet.GSSSessionMgr.SSServerConnect(verify, remote, enet.BigSendChannelSize)
	}
}

var GServer *Server
