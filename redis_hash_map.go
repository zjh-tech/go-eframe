package eframe

import (
	"github.com/go-redis/redis"
)

type RedisHashMap struct {
	cacheKey string
	redisCmd redis.Cmdable
}

func NewRedisHashMap(cacheKey string, redisCmd redis.Cmdable) *RedisHashMap {
	return &RedisHashMap{
		cacheKey: cacheKey,
		redisCmd: redisCmd,
	}
}

// 设置数据
func (r *RedisHashMap) SetData(key string, value interface{}) error {
	_, err := r.redisCmd.HSet(r.cacheKey, key, value).Result()
	return err
}

// 设置多条数据
func (r *RedisHashMap) SetDatas(fields map[string]interface{}) error {
	_, err := r.redisCmd.HMSet(r.cacheKey, fields).Result()
	return err
}

// 获取数据
func (r *RedisHashMap) GetData(key string) (string, error) {
	return r.redisCmd.HGet(r.cacheKey, key).Result()
}

// 获取多个数据
func (r *RedisHashMap) GetDatas(keys []string) ([]interface{}, error) {
	return r.redisCmd.HMGet(r.cacheKey, keys...).Result()
}

// 获取所有数据
func (r *RedisHashMap) GetAllDatas() (map[string]string, error) {
	return r.redisCmd.HGetAll(r.cacheKey).Result()
}

// 删除数据
func (r *RedisHashMap) DelData(key string) error {
	_, err := r.redisCmd.HDel(r.cacheKey, key).Result()
	return err
}

// 删除多个数据
func (r *RedisHashMap) DelDatas(keys []string) error {
	_, err := r.redisCmd.HDel(r.cacheKey, keys...).Result()
	return err
}

// 删除所有数据
func (r *RedisHashMap) DelAllDatas() error {
	_, err := r.redisCmd.Del(r.cacheKey).Result()
	return err
}

// 是否存在
func (r *RedisHashMap) IsExist(key string) (bool, error) {
	return r.redisCmd.SIsMember(r.cacheKey, key).Result()
}
