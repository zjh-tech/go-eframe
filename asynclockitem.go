package eframe

import (
	"sync"

	"gitee.com/zjh-tech/go-ebase/util"
)

//Client Req Too Much, Local Req Is Async Operator

type AsyncLockItem struct {
	Lockkey       int
	LockStrKey    string
	StartLockTime int64
	EndLockTime   int64
}

type AsyncLockItemMgr struct {
	lockMap    map[int]*AsyncLockItem
	lockStrMap map[string]*AsyncLockItem
	safeFlag   bool
	mutex      sync.Mutex
}

func NewAsyncLockItemMgr(safeFlag bool) *AsyncLockItemMgr {
	return &AsyncLockItemMgr{
		lockMap:    make(map[int]*AsyncLockItem),
		lockStrMap: make(map[string]*AsyncLockItem),
		safeFlag:   safeFlag,
	}
}

func (a *AsyncLockItemMgr) Lock(lockkey int, locktime int64) bool {
	if a.safeFlag {
		a.mutex.Lock()
		defer a.mutex.Unlock()
	}

	now := util.GetSecond()
	if info, ok := a.lockMap[lockkey]; ok {
		if info.EndLockTime > now {
			return true
		}
		info.StartLockTime = now
		info.EndLockTime = now + locktime
		return false
	}

	item := &AsyncLockItem{
		Lockkey:       lockkey,
		StartLockTime: now,
		EndLockTime:   now + locktime,
	}
	a.lockMap[lockkey] = item
	return false
}

func (a *AsyncLockItemMgr) UnLock(lockkey int) {
	if a.safeFlag {
		a.mutex.Lock()
		defer a.mutex.Unlock()
	}

	delete(a.lockMap, lockkey)
}

func (a *AsyncLockItemMgr) LockString(lockstrkey string, locktime int64) bool {
	if a.safeFlag {
		a.mutex.Lock()
		defer a.mutex.Unlock()
	}

	now := util.GetSecond()
	if info, ok := a.lockStrMap[lockstrkey]; ok {
		if info.EndLockTime < now {
			return true
		}
		info.StartLockTime = now
		info.EndLockTime = now + locktime
		return false
	}

	item := &AsyncLockItem{
		LockStrKey:    lockstrkey,
		StartLockTime: now,
		EndLockTime:   now + locktime,
	}
	a.lockStrMap[lockstrkey] = item
	return false
}

func (a *AsyncLockItemMgr) UnLockString(lockstrkey string) {
	if a.safeFlag {
		a.mutex.Lock()
		defer a.mutex.Unlock()
	}

	delete(a.lockStrMap, lockstrkey)
}

var GAsyncLockItemMgr *AsyncLockItemMgr

const (
	ASYNC_LOCK_ONE_SEC   int64 = 1
	ASYNC_LOCK_TWO_SEC   int64 = 2
	ASYNC_LOCK_THREE_SEC int64 = 3
	ASYNC_LOCK_FOUR_SEC  int64 = 4
	ASYNC_LOCK_FIVE_SEC  int64 = 5
	ASYNC_LOCK_SIX_SEC   int64 = 6
	ASYNC_LOCK_SEVEN_SEC int64 = 7
	ASYNC_LOCK_EIGHT_SEC int64 = 8
	ASYNC_LOCK_NINE_SEC  int64 = 9
)
