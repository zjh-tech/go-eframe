package eframe

import (
	"errors"
	"sync"
	"time"
)

//10位数的时间戳是以 秒 为单位       1530027865
//13位数的时间戳是以 毫秒 为单位     1530027865231
//19位数的时间戳是以 纳秒 为单位	 1530027865231834600

//1年=31536000000毫秒
//2199023255552/31536000000=69.7年

//2^(64-1-22)    [0-2199023255552]
//2^(22-10)      [0-4095]
//2^10)          [0-1023]

//1683536023285 - 1420041600000 = 符号位 | 263,494,423,285 << 22 | 1001 << 10 | 0 = 1105175713562993664
//(0000符号位) 1111 0101 0110 0101 1111 0111 0100 1011 1101 0100 1111 1010 0100 0000 0000  ==> 0
//1111 0101 0110 0101 1111 0111 0100 1011 1101 0100 1111 1010 01  ==> 1001
//1111 0101 0110 0101 1111 0111 0100 1011 1101 01  ==> 263494423285

const (
	//t := time.Date(2015, 1, 1, 00, 00, 00, 00, time.Local).UnixNano() / 1e6;//获取时间戳 毫秒
	//41位的时间截，可以使用69年，年T = (1L << 41) / (1000L * 60 * 60 * 24 * 365) = 69<br>
	epoch        int64 = 1420041600000
	serverIdBits int64 = 12
	//0-4095
	maxServerId  int64 = -1 ^ (-1 << serverIdBits)
	sequenceBits int64 = 10
	//0-1023
	sequenceMask int64 = -1 ^ (-1 << sequenceBits)
	// 数据标识id向左移10位
	serverIdShift int64 = sequenceBits
	// 时间截向左移22位
	timestampLeftShift int64 = sequenceBits + serverIdBits
)

type IdMaker struct {
	mutex         sync.Mutex // 添加互斥锁 确保并发安全
	safeFlag      bool
	lastTimestamp int64 // 上次生成ID的时间截
	serverId      int64
	sequence      int64 // 毫秒内序列(0~4095)
}

// (0-4095)
func NewIdMaker(serverId int64, safeFlag bool) (*IdMaker, error) {
	if serverId < 0 || serverId > maxServerId {
		return nil, errors.New("Server ID excess of quantity")
	}
	return &IdMaker{
		lastTimestamp: 0,
		serverId:      serverId,
		sequence:      0,
		safeFlag:      safeFlag,
	}, nil
}

func (m *IdMaker) nextId() int64 {
	now := time.Now().UnixNano() / 1e6
	if now < m.lastTimestamp {
		panic("Clock moved backwards")
	}
	if m.lastTimestamp == now {
		m.sequence = (m.sequence + 1) & sequenceMask
		if m.sequence == 0 {
			// 阻塞到下一个毫秒，直到获得新的时间戳
			for now <= m.lastTimestamp {
				now = time.Now().UnixNano() / 1e6
			}
		}
	} else {
		m.sequence = 0
	}

	m.lastTimestamp = now
	//1(0为正数,不用) + 41(41位的时间截，可以使用69年) + 12(4095) + 10(1023) = 64位
	ID := int64((now-epoch)<<timestampLeftShift | m.serverId<<serverIdShift | m.sequence)
	return ID
}

func (m *IdMaker) NextId() int64 {
	if m.safeFlag {
		m.mutex.Lock()
		defer m.mutex.Unlock()
	}

	return m.nextId()
}

func (m *IdMaker) NextIds(num int) []int64 {
	ids := make([]int64, num)
	if m.safeFlag {
		m.mutex.Lock()
		defer m.mutex.Unlock()
	}

	for i := 0; i < num; i++ {
		ids[i] = m.nextId()
	}
	return ids
}

var GIdMaker *IdMaker
