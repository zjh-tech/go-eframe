package eframe

import (
	"context"
	"time"

	"github.com/pkg/errors"
	clientv3 "go.etcd.io/etcd/client/v3"
)

var GEtcdClient *clientv3.Client

func ConnectEtcd(addrs []string, timeout time.Duration) (*clientv3.Client, error) {
	clientConfig := clientv3.Config{
		Endpoints:   addrs,
		DialTimeout: timeout,
	}

	client, err := clientv3.New(clientConfig)

	timeoutCtx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err = client.Status(timeoutCtx, clientConfig.Endpoints[0])
	if err != nil {
		return nil, errors.Wrapf(err, "error checking etcd status: %v", err)
	}

	return client, err
}
