package eframe

type LbSpec struct {
	ServiceID   uint64 `json:"service_id"`
	EtcdPrefix  string `json:"etcd_prefix"`
	ServiceType uint32 `json:"service_type"`
	Addr        string `json:"addr"`
	State       uint32 `json:"state"`
}

type LbRegisterReq struct {
	LbInfo *LbSpec `json:"server_spec"`
}

type LbRegisterRes struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type SelectMinServiceReq struct {
	EtcdPrefix  string `json:"etcd_prefix"`
	ServiceType uint32 `json:"service_type"`
}

type SelectMinServiceRes struct {
	Code        int    `json:"code"`
	Message     string `json:"message"`
	ServiceID   uint64 `json:"service_id"`
	EtcdPrefix  string `json:"etcd_prefix"`
	ServiceType uint32 `json:"service_type"`
	Addr        string `json:"addr"`
}

const (
	LbRegisterOk               int = 0
	LbRegisterInputParasFailed int = 1
)

const (
	SelectMinServiceOk        int = 0
	SelectMinInputParasFailed int = 1
	SelectMinServiceFail      int = 2
)
