package eframe

import (
	"context"

	clientv3 "go.etcd.io/etcd/client/v3"
)

type EtcdQueueHandlerFunc func(key string, val string)
type DelEtcdQueueHandlerFunc func(key string)

type EtcdMsgChannel struct {
	Key     string
	Val     string
	DelFlag bool
}

func NewEtcdMsgChannel(key string, val string) *EtcdMsgChannel {
	return &EtcdMsgChannel{
		Key: key,
		Val: val,
	}
}

func NewDelEtcdMsgChannel(key string) *EtcdMsgChannel {
	return &EtcdMsgChannel{
		Key:     key,
		DelFlag: true,
	}
}

const (
	EtcdMsgChannelMaxSize = 100000
)

type EtcdQueue struct {
	cli *clientv3.Client

	msgChan chan *EtcdMsgChannel

	handlerFunc    EtcdQueueHandlerFunc
	delHandlerFunc DelEtcdQueueHandlerFunc
}

func NewEtcdQueue(cli *clientv3.Client) *EtcdQueue {
	return &EtcdQueue{
		cli:     cli,
		msgChan: make(chan *EtcdMsgChannel, EtcdMsgChannelMaxSize),
	}
}

func (e *EtcdQueue) Init(handlerFunc EtcdQueueHandlerFunc, delHandlerFunc DelEtcdQueueHandlerFunc) bool {
	e.handlerFunc = handlerFunc
	e.delHandlerFunc = delHandlerFunc
	return true
}

func (e *EtcdQueue) PublishMsg(key, val string) error {
	if _, err := e.cli.Put(context.Background(), key, val); err != nil {
		return err
	}
	return nil
}

func (e *EtcdQueue) SubscribeMsg(prefix string) error {
	resp, err := e.cli.Get(context.Background(), prefix, clientv3.WithPrefix())
	if err != nil {
		return err
	}

	for _, kv := range resp.Kvs {
		e.msgChan <- NewEtcdMsgChannel(string(kv.Key), string(kv.Value))
	}

	go func() {
		ELog.Infof("ServiceDiscovery Watching Prefix:%s Now... ", prefix)
		rch := e.cli.Watch(context.Background(), prefix, clientv3.WithPrefix())
		for wresp := range rch {
			for _, ev := range wresp.Events {
				switch ev.Type {
				case clientv3.EventTypePut:
					e.msgChan <- NewEtcdMsgChannel(string(ev.Kv.Key), string(ev.Kv.Value))
				case clientv3.EventTypeDelete:
					e.msgChan <- NewDelEtcdMsgChannel(string(ev.Kv.Key))
				}
			}
		}
	}()

	return nil
}

func (e *EtcdQueue) GetTriggerChannel() chan *EtcdMsgChannel {
	return e.msgChan
}

func (e *EtcdQueue) ExecTriggerLogic(evt *EtcdMsgChannel) {
	if evt.DelFlag {
		if e.delHandlerFunc != nil {
			e.delHandlerFunc(evt.Key)
		}
	} else {
		if e.handlerFunc != nil {
			e.handlerFunc(evt.Key, evt.Val)
		}
	}
}
