package eframe

import "time"

const (
	MeterLimitMillSec = 100
)

type EctdServiceSpec struct {
	ServerId      uint64
	ServerType    uint32
	ServerTypeStr string
	Token         string
	Inner         string
	Outer         string
}

const (
	MillSecond int64 = 1
	Second     int64 = MillSecond * 1000
	Minute     int64 = Second * 60
	Hour       int64 = Minute * 60
	Day        int64 = Hour * 24
)

const MillSecondDuration = time.Millisecond
const SecondDuration = time.Second

const (
	APP_LOG_PATH  = "./log"
	APP_LOG_LEVEL = 1
)

const (
	MSG_SUCCESS_ERRORCODE uint32 = 0
	MSG_FAIL_ERRORCODE    uint32 = 1
)

const (
	ETCD_CONNECT_TIMEOUT            time.Duration = time.Second * 60 * 5 //5分钟
	SERVICE_REGISTER_ETCD_LEASE_SEC int64         = 60 * 3               //租约时间
)
