package eframe

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"strings"

	"github.com/nats-io/nats.go"
	"google.golang.org/protobuf/proto"
)

const (
	NetsMsgIDLen       int = 4
	NetsMsgChannelSize int = 100000
)

type INatsMsgOnHandler interface {
	OnHandler(msgId uint32, datas []byte)
}

type NatsMessageQueue struct {
	natsConn   *nats.Conn
	msgs       chan []byte
	msghandler INatsMsgOnHandler
}

func NewNatsMessageQueue() *NatsMessageQueue {
	return &NatsMessageQueue{}
}

// url: nats://ip:port
func (n *NatsMessageQueue) Init(msghandler INatsMsgOnHandler, urls []string, options ...nats.Option) error {
	natsConn, err := nats.Connect(strings.Join(urls, ","), options...)
	if err != nil {
		return err
	}
	n.natsConn = natsConn
	n.msgs = make(chan []byte, NetsMsgChannelSize)
	n.msghandler = msghandler
	return nil
}

func (n *NatsMessageQueue) UnInit() {
	if n.natsConn != nil {
		n.natsConn.Close()
		n.natsConn = nil
	}
}

// 发布
// 向ServerID的服务器发消息
func (n *NatsMessageQueue) PublishMsgByServerID(serverType string, serverId uint64, msgId uint32, msg proto.Message) error {
	subj := fmt.Sprintf("%s.%v", serverType, serverId) //example: game.101
	datas, err := n.marshalNatsMsg(msgId, msg)
	if err != nil {
		return err
	}
	return n.natsConn.Publish(subj, datas)
}

// 向某类型的服务器广播消息
func (n *NatsMessageQueue) PublishBroadcastMsg(serverType string, msgId uint32, msg proto.Message) error {
	subj := fmt.Sprintf("%s.all", serverType) //example: game.all
	datas, err := n.marshalNatsMsg(msgId, msg)
	if err != nil {
		return err
	}
	return n.natsConn.Publish(subj, datas)
}

// 向莫类型的服务器随机一个发消息
func (n *NatsMessageQueue) PublishMsgByRandom(serverType string, msgId uint32, msg proto.Message) error {
	subj := fmt.Sprintf("%s.random", serverType) //example: game.random
	datas, err := n.marshalNatsMsg(msgId, msg)
	if err != nil {
		return err
	}
	return n.natsConn.Publish(subj, datas)
}

// 订阅
// 接受PublishMsgByServerID的消息
func (n *NatsMessageQueue) SubscribeMsgByServerID(serverType string, serverId uint64) {
	subj := fmt.Sprintf("%s.%v", serverType, serverId) //example: game.101
	n.natsConn.Subscribe(subj, func(msg *nats.Msg) {
		n.msgs <- msg.Data
	})
}

// 接受PublishBroadcastMsg的消息
func (n *NatsMessageQueue) SubscribeBroadcastMsg(serverType string) {
	subj := fmt.Sprintf("%s.all", serverType) //example: game.all
	n.natsConn.Subscribe(subj, func(msg *nats.Msg) {
		n.msgs <- msg.Data
	})
}

// 接受PublishMsgByRandom消息
func (n *NatsMessageQueue) SubscribeQueueMsg(serverType string, queue string) {
	subj := fmt.Sprintf("%s.random", serverType) //example: game.random
	n.natsConn.QueueSubscribe(subj, queue, func(msg *nats.Msg) {
		n.msgs <- msg.Data
	})
}

// 向某类型服务器上的玩家发送消息
func (n *NatsMessageQueue) PublishMsgByRoleID(serverType string, roleId uint64, msgId uint32, msg proto.Message) error {
	subj := fmt.Sprintf("%s.%v", serverType, roleId) //example: game.roleid
	datas, err := n.marshalNatsMsg(msgId, msg)
	if err != nil {
		return err
	}
	return n.natsConn.Publish(subj, datas)
}

// 接受PublishMsgByRoleID的消息
func (n *NatsMessageQueue) SubscribeMsgByRoleID(serverType string, roleId uint64) (*nats.Subscription, error) {
	subj := fmt.Sprintf("%s.%v", serverType, roleId) //example: game.roleid
	subscription, err := n.natsConn.Subscribe(subj, func(msg *nats.Msg) {
		n.msgs <- msg.Data
	})

	if err != nil {
		return nil, fmt.Errorf("%v SubscribeMsgByRoleID Error=%v", roleId, err)
	}
	return subscription, nil
}

// 取消订阅: SubscribeMsgByRoleID返回的Subscription
func (n *NatsMessageQueue) UnsubscribeByRoleID(subscription *nats.Subscription) error {
	if err := subscription.Unsubscribe(); err != nil {
		return fmt.Errorf("UnsubscribeByRoleID Error=%v", err)
	}

	return nil
}

func (n *NatsMessageQueue) GetTriggerChannel() chan []byte {
	return n.msgs
}

func (n *NatsMessageQueue) ExecTriggerLogic(datas []byte) {
	msgId, bodyBytes, err := n.unMarshalNatsMsg(datas)
	if err != nil {
		return
	}
	n.msghandler.OnHandler(msgId, bodyBytes)
}

func (n *NatsMessageQueue) marshalNatsMsg(msgId uint32, msg proto.Message) ([]byte, error) {
	datas, err := proto.Marshal(msg)
	if err != nil {
		ELog.Errorf("[NatsMessageQueue] MarshalNatsMsg MsgID=%v Proto.Marshal Err=%v", msgId, err)
		return nil, err
	}

	bodyBuff := bytes.NewBuffer([]byte{})
	if err := binary.Write(bodyBuff, binary.BigEndian, msgId); err != nil {
		ELog.Errorf("[NatsMessageQueue] MarshalNatsMsg MsgID=%v Error=%v", msg, err)
		return nil, err
	}

	if len(datas) != 0 {
		if err := binary.Write(bodyBuff, binary.BigEndian, datas); err != nil {
			ELog.Errorf("[NatsMessageQueue] MarshalNatsMsg MsgID=%v Datas Error=%v", msgId, err)
			return nil, err
		}
	}

	bodyBytes := bodyBuff.Bytes()
	bodyLen := uint32(len(bodyBytes))

	buff := bytes.NewBuffer([]byte{})
	if err := binary.Write(buff, binary.BigEndian, bodyLen); err != nil {
		ELog.Errorf("[NatsMessageQueue] MarshalNatsMsg MsgID=%v BodyLen Error=%v", msg, err)
		return nil, err
	}
	if err := binary.Write(buff, binary.BigEndian, bodyBytes); err != nil {
		ELog.Errorf("[NatsMessageQueue] MarshalNatsMsg MsgID=%v BodyBytes Error=%v", msg, err)
		return nil, err
	}

	return buff.Bytes(), nil
}

func (n *NatsMessageQueue) unMarshalNatsMsg(datas []byte) (uint32, []byte, error) {
	if len(datas) < NetsMsgIDLen {
		return 0, nil, errors.New("[NatsMessageQueue] UnMarshalNatsMsg Less Than MsgIDLen")
	}
	buff := bytes.NewBuffer(datas)
	msgId := uint32(0)
	if err := binary.Read(buff, binary.BigEndian, &msgId); err != nil {
		ELog.Errorf("[NatsMessageQueue] UnMarshalNatsMsg MsgID Error=%v", err)
		return 0, nil, err
	}
	return msgId, datas[NetsMsgIDLen:], nil
}
