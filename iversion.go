package eframe

type IVersion interface {
	GetVersion() string
}
