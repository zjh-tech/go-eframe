package eframe

import (
	"gitee.com/zjh-tech/go-edb"
)

type ServerSpec struct {
	ServerId        uint64 `yaml:"serverid"`
	ServerTypeStr   string `yaml:"servertype"`
	Token           string `yaml:"token"`
	Inner           string `yaml:"inner"`
	Outer           string `yaml:"outer"`
	EtcdPrefix      string `yaml:"etcdprefix"`
	ExtraConfigPath string `yaml:"extraconfigpath"`
}

type LogSpec struct {
	Path  string
	Level int
}

type EtcdSpec struct {
	WatchServerList string          `yaml:"watchserverlist"`
	AddrList        []*EtcdConnSpec `yaml:"addrlist"`
}

type RedisConnSpec struct {
	Addr string `yaml:"addr"`
}

type RedisServerSpec struct {
	ServerId uint64 `yaml:"serverid"`
}

type RedisSpec struct {
	Cluster          int                `yaml:"cluster"`
	RedisServerSpecs []*RedisServerSpec `yaml:"serverids"`
	Password         string             `yaml:"password"`
	RedisConnSpecs   []*RedisConnSpec   `yaml:"addrs"`
}

type DBSpec struct {
	TableMaxCount uint64            `yaml:"tablemaxcount"`
	DBInfoList    []*edb.DBConnSpec `yaml:"dblist"`
}

type HttpSpec struct {
	Url       string `yaml:"url"`
	HttpsFlag int    `yaml:"https_flag"`
	CertFile  string `yaml:"cert_file"`
	KeyFile   string `yaml:"key_file"`
}

type MsClientTcpSpec struct {
	Addr  string `yaml:"addr"`
	Token string `yaml:"token"`
}

type MsServerTcpSpec struct {
	Addr  string `yaml:"addr"`
	Token string `yaml:"token"`
}

type TcpSpec struct {
	InnerAddr        string `yaml:"inneraddr"`
	OuterAddr        string `yaml:"outeraddr"`
	OpenoverLoad     int    `yaml:"openoverload"`
	innervaltime     int64  `yaml:"innervaltime"`
	Limit            int64  `yaml:"limit"`
	BeatHeartMaxTime int64  `yaml:"beatheart_maxtime"`
}

func (t *TcpSpec) IsOpenOverLoad() bool {
	return t.OpenoverLoad != 0
}

type WebSocketSpec struct {
	InnerUrl         string `yaml:"inner_url"`
	OutUrl           string `yaml:"out_url"`
	OpenoverLoad     int    `yaml:"openoverload"`
	Innervaltime     int64  `yaml:"innervaltime"`
	Limit            int64  `yaml:"limit"`
	BeatHeartMaxTime int64  `yaml:"beatheart_maxtime"`
}

func (w *WebSocketSpec) IsOpenOverLoad() bool {
	return w.OpenoverLoad != 0
}

type UdpSpec struct {
	InnerAddr    string `yaml:"inneraddr"`
	OuterAddr    string `yaml:"outeraddr"`
	DataShards   int    `yaml:"datashards"`
	ParityShards int    `yaml:"parityshards"`
}

type ServerCfg struct {
	ServerInfo      ServerSpec
	LogInfo         LogSpec
	WatchServerList []string
	RedisInfo       *RedisSpec
	DBInfo          *DBSpec
	LbOuterAddr     string
}

func (s *ServerCfg) GetRedisAddrs() []string {
	addrs := make([]string, 0)
	for _, info := range s.RedisInfo.RedisConnSpecs {
		addrs = append(addrs, info.Addr)
	}
	return addrs
}

func (s *ServerCfg) IsOpenRedis() bool {
	return s.RedisInfo != nil
}

func (s *ServerCfg) IsOpenRedisCluster() bool {
	return s.RedisInfo.Cluster != 0
}

func (s *ServerCfg) IsOpenDB() bool {
	return s.DBInfo != nil
}

var GServerCfg *ServerCfg

func init() {
	GServerCfg = &ServerCfg{}
}
