package eframe

const (
	LogDebug = iota
	LogInfo
	LogWarn
	LogError
)

type ILog interface {
	Debug(v ...interface{})
	Debugf(format string, v ...interface{})

	Info(v ...interface{})
	Infof(format string, v ...interface{})

	Warn(v ...interface{})
	Warnf(format string, v ...interface{})

	Error(v ...interface{})
	Errorf(format string, v ...interface{})
}

type ComLogger struct {
	log   ILog
	level int
}

func NewComLogger() *ComLogger {
	return &ComLogger{}
}

func (c *ComLogger) Init(log ILog, level int) {
	c.log = log
	c.level = level
}

func (c *ComLogger) Debug(v ...interface{}) {
	if c.level <= LogDebug {
		c.log.Debug(v...)
	}
}

func (c *ComLogger) Debugf(format string, v ...interface{}) {
	if c.level <= LogDebug {
		c.log.Debugf(format, v...)
	}
}

func (c *ComLogger) Info(v ...interface{}) {
	if c.level <= LogInfo {
		c.log.Info(v...)
	}
}

func (c *ComLogger) Infof(format string, v ...interface{}) {
	if c.level <= LogInfo {
		c.log.Infof(format, v...)
	}
}

func (c *ComLogger) Warn(v ...interface{}) {
	if c.level <= LogWarn {
		c.log.Warn(v...)
	}
}

func (c *ComLogger) Warnf(format string, v ...interface{}) {
	if c.level <= LogWarn {
		c.log.Warnf(format, v...)
	}
}

func (c *ComLogger) Error(v ...interface{}) {
	if c.level <= LogError {
		c.log.Error(v...)
	}
}

func (c *ComLogger) Errorf(format string, v ...interface{}) {
	if c.level <= LogError {
		c.log.Errorf(format, v...)
	}
}

var ELog *ComLogger

func init() {
	ELog = NewComLogger()
}
