package eframe

type EngineLogInfo struct {
	TimerLevel int `yaml:"timerlevel"`
	DbLevel    int `yaml:"dblevel"`
	NetLevel   int `yaml:"netlevel"`
	RedisLevel int `yaml:"redislevel"`
	FrameLevel int `yaml:"framelevel"`
}
