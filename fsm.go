package eframe

import (
	"sync"
)

type FSMState uint32 //状态
const (
	IDLE_STATE FSMState = 0
)

type FSMEvent uint32                               //事件
type FSMHandler func(paras []interface{}) FSMState //处理方法,并返回新的状态

type GetFSMStateStringFunc func(state FSMState, event FSMEvent) string

// 有限状态机
type FSM struct {
	mutex    sync.Mutex
	safeFlag bool
	state    FSMState                             //当前状态
	handlers map[FSMState]map[FSMEvent]FSMHandler //处理集，每一个状态都可以出发有限个事件，执行有限个处理

	stateStrs   map[FSMState]string //状态描述
	eventStrs   map[FSMEvent]string //事件描述
	openLogFlag bool
}

func NewFSM(initState FSMState, safeFlag bool) *FSM {
	return &FSM{
		state:       initState,
		safeFlag:    safeFlag,
		handlers:    make(map[FSMState]map[FSMEvent]FSMHandler),
		stateStrs:   make(map[FSMState]string),
		eventStrs:   make(map[FSMEvent]string),
		openLogFlag: false,
	}
}

func (f *FSM) SetFsmLog(flag bool) {
	f.openLogFlag = flag
}

func (f *FSM) AddHandler(state FSMState, event FSMEvent, handler FSMHandler) *FSM {
	if f.safeFlag {
		f.mutex.Lock()
		defer f.mutex.Unlock()
	}

	if _, ok := f.handlers[state]; !ok {
		f.handlers[state] = make(map[FSMEvent]FSMHandler)
	}

	if _, ok := f.handlers[state][event]; ok {
		ELog.Warnf("[FSM] AddHandler %v", f.GetStateStr(state), f.GetEventStr(event))
	}
	f.handlers[state][event] = handler
	return f
}

func (f *FSM) AddStateDesc(stateDescs map[FSMState]string) {
	if f.safeFlag {
		f.mutex.Lock()
		defer f.mutex.Unlock()
	}

	for state, desc := range stateDescs {
		f.stateStrs[state] = desc
	}
}

func (f *FSM) AddEventDesc(eventDescs map[FSMEvent]string) {
	if f.safeFlag {
		f.mutex.Lock()
		defer f.mutex.Unlock()
	}

	for event, desc := range eventDescs {
		f.eventStrs[event] = desc
	}
}

func (f *FSM) Call(event FSMEvent, paras []interface{}) FSMState {
	if f.safeFlag {
		f.mutex.Lock()
		defer f.mutex.Unlock()
	}

	events := f.handlers[f.GetState()]
	if events == nil {
		return f.GetState()
	}
	if fn, ok := events[event]; ok {
		oldState := f.GetState()
		newState := fn(paras)
		f.SetState(newState)
		if f.openLogFlag {
			ELog.Infof("[FSM] State From [%v] To [%v] TriggerEvent = [%v]", f.GetStateStr(oldState), f.GetStateStr(newState), f.GetEventStr(event))
		}
	}
	return f.GetState()
}

func (f *FSM) GetState() FSMState {
	return f.state
}

func (f *FSM) SetState(newState FSMState) {
	f.state = newState
	if f.openLogFlag {
		ELog.Infof("[FSM] SetState [%v]", f.GetStateStr(newState))
	}
}

func (f *FSM) GetStateStr(state FSMState) string {
	if str, ok := f.stateStrs[state]; ok {
		return str
	}

	return "Unknow State"
}

func (f *FSM) GetEventStr(event FSMEvent) string {
	if str, ok := f.eventStrs[event]; ok {
		return str
	}

	return "Unknow Event"
}
