package eframe

import (
	"context"
	"sync"
	"time"

	clientv3 "go.etcd.io/etcd/client/v3"
)

type ServiceDiscoveryAddFunc func(key string, val string)
type ServiceDiscoveryDelFunc func(key string, val string)
type ServiceDiscoveryUpdFunc func([]string)

type ServiceChannel struct {
	AddFlag bool
	DelFlag bool
	UpdFlag bool
	Key     string
	Val     string
	Vals    []string
}

const (
	ServiceChannelMaxSize = 10000
)

func newAddServiceChannel(key string, val string) *ServiceChannel {
	return &ServiceChannel{
		AddFlag: true,
		Key:     key,
		Val:     val,
	}
}

func newDelServiceChannel(key string, val string) *ServiceChannel {
	return &ServiceChannel{
		DelFlag: true,
		Key:     key,
		Val:     val,
	}
}

func newUpdServiceChannel(vals []string) *ServiceChannel {
	return &ServiceChannel{
		UpdFlag: true,
		Vals:    vals,
	}
}

type ServiceDiscovery struct {
	cli *clientv3.Client

	serviceList sync.Map
	//例子: "/service/ms/relation/" -"{\"ServerId\":3961,\"ServerType\":113,\"ServerTypeStr\":\"relation\",\"Token\":\"\",\"Inter\":\"0.0.0.0:60301\",\"Outer\":\"\"}"

	addFunc ServiceDiscoveryAddFunc
	delFunc ServiceDiscoveryDelFunc
	updFunc ServiceDiscoveryUpdFunc

	serviceChan chan *ServiceChannel
}

func newServiceDiscovery() *ServiceDiscovery {
	return &ServiceDiscovery{
		serviceChan: make(chan *ServiceChannel, ServiceChannelMaxSize),
	}
}

func (s *ServiceDiscovery) Init(ctxWithCancel context.Context, cli *clientv3.Client, addFunc ServiceDiscoveryAddFunc, delFunc ServiceDiscoveryDelFunc, updFunc ServiceDiscoveryUpdFunc) bool {
	if (cli == nil) || (addFunc == nil) || (delFunc == nil) || (updFunc == nil) {
		return false
	}

	s.cli = cli
	s.addFunc = addFunc
	s.delFunc = delFunc
	s.updFunc = updFunc

	go func() {
		updTimer := time.NewTicker(30 * time.Second)
		defer updTimer.Stop()

		for {
			select {
			case <-updTimer.C:
				{
					vals := s.getServiceList()
					s.serviceChan <- newUpdServiceChannel(vals)
				}
			case <-ctxWithCancel.Done():
				return
			}
		}
	}()

	return true
}

func (s *ServiceDiscovery) WatchService(parentCtx context.Context, prefix string) error {
	resp, err := s.cli.Get(parentCtx, prefix, clientv3.WithPrefix())
	if err != nil {
		return err
	}

	for _, kv := range resp.Kvs {
		s.setServiceList(string(kv.Key), string(kv.Value))
	}

	go func() {
		ELog.Infof("ServiceDiscovery Watching Prefix:%s Now... ", prefix)
		rch := s.cli.Watch(context.Background(), prefix, clientv3.WithPrefix())
		for wresp := range rch {
			for _, ev := range wresp.Events {
				switch ev.Type {
				case clientv3.EventTypePut:
					s.setServiceList(string(ev.Kv.Key), string(ev.Kv.Value))
				case clientv3.EventTypeDelete:
					s.delServiceList(string(ev.Kv.Key))
				}
			}
		}
	}()

	return nil
}

func (s *ServiceDiscovery) getServiceList() []string {
	addrs := make([]string, 0)
	s.serviceList.Range(func(k, v interface{}) bool {
		addrs = append(addrs, v.(string))
		return true
	})
	return addrs
}

func (s *ServiceDiscovery) setServiceList(key, val string) {
	s.serviceList.Store(key, val)
	ELog.Infof("ServiceDiscovery Put Key:%v Val:%v", key, val)
	s.serviceChan <- newAddServiceChannel(key, val)
}

func (s *ServiceDiscovery) delServiceList(key string) {
	ELog.Infof("ServiceDiscovery Del Key:%v ", key)
	val, delFlag := s.serviceList.LoadAndDelete(key)
	if !delFlag {
		return
	}
	s.serviceChan <- newDelServiceChannel(key, val.(string))
}

func (s *ServiceDiscovery) Run() {
	for {
		select {
		case evt, ok := <-s.GetTriggerChannel():
			if ok {
				s.ExecTriggerLogic(evt)
			}
		default:
			return
		}
	}
}

func (s *ServiceDiscovery) GetTriggerChannel() chan *ServiceChannel {
	return s.serviceChan
}

func (s *ServiceDiscovery) ExecTriggerLogic(evt *ServiceChannel) {
	if evt.AddFlag {
		s.addFunc(evt.Key, evt.Val)
	} else if evt.DelFlag {
		s.delFunc(evt.Key, evt.Val)
	} else if evt.UpdFlag {
		s.updFunc(evt.Vals)
	} else {
		ELog.Error("ServiceDiscovery Event Type Error")
	}
}

var GServiceDiscovery *ServiceDiscovery

func init() {
	GServiceDiscovery = newServiceDiscovery()
}
