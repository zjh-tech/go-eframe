package eframe

import (
	"fmt"

	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/reflect/protoreflect"
	"google.golang.org/protobuf/reflect/protoregistry"
)

type ProtoCreator struct {
	fullNames map[uint32]protoreflect.FullName
}

func NewProtoCreator() *ProtoCreator {
	return &ProtoCreator{
		fullNames: make(map[uint32]protoreflect.FullName),
	}
}

func (p *ProtoCreator) Register(msgId uint32, msg proto.Message) error {
	fullNames := msg.ProtoReflect().Descriptor().FullName()
	if _, ok := p.fullNames[msgId]; ok {
		return fmt.Errorf("MsgID Exist %v", msgId)
	}

	p.fullNames[msgId] = fullNames
	return nil
}

func (p *ProtoCreator) GetObject(msgId uint32, datas []byte) (proto.Message, error) {
	fullName, ok := p.fullNames[msgId]
	if !ok {
		return nil, fmt.Errorf("MsgID Not Exist %v", msgId)
	}

	msgType, err := protoregistry.GlobalTypes.FindMessageByName(fullName)
	if err != nil {
		return nil, fmt.Errorf("MsgID=%v FindMessageByName Error=%v", msgId, err)
	}
	msg := msgType.New().Interface()
	err = proto.Unmarshal(datas, msg)
	if err != nil {
		return nil, fmt.Errorf("MsgID=%v Unmarshal Error=%v", msgId, err)
	}
	return msg, nil
}

// func TestFullName() {
// 	msgId := uint32(1)
// 	creator := NewProtoCreator()
// 	creator.Register(msgId, &config.C2SGmReq{})

// 	req := &config.C2SGmReq{
// 		Content: "1234",
// 	}
// 	datas, _ := proto.Marshal(req)
// 	tempReq, _ := creator.GetObject(msgId, datas)
// 	gmReq := tempReq.(*config.C2SGmReq)
// 	fmt.Println(gmReq.String())
// }
