package eframe

import (
	"context"
	"time"

	clientv3 "go.etcd.io/etcd/client/v3"
)

type ServiceRegister struct {
	cli *clientv3.Client

	key   string
	val   string
	lease int64

	leaseId       clientv3.LeaseID
	keepAliveChan <-chan *clientv3.LeaseKeepAliveResponse

	keepAliveFlag bool
}

func newServiceRegister() *ServiceRegister {
	return &ServiceRegister{}
}

func (s *ServiceRegister) Init(cli *clientv3.Client) bool {
	if cli == nil {
		return false
	}

	s.cli = cli
	return true
}

func (s *ServiceRegister) StartService(key, val string, lease int64) error {
	s.key = key
	s.val = val
	s.lease = lease

	if err := s.connect(); err != nil {
		return err
	}
	ELog.Infof("ServiceRegister %v %v StartService Success! ", s.key, s.val)
	return nil
}

func (s *ServiceRegister) connect() error {
	//设置租约时间
	if resp, err := s.cli.Grant(context.Background(), s.lease); err != nil {
		return err
	} else {
		s.leaseId = resp.ID
	}

	//注册服务并绑定租约
	if _, err := s.cli.Put(context.Background(), s.key, s.val, clientv3.WithLease(s.leaseId)); err != nil {
		return err
	}

	//设置续租,定期发送需求请求
	if respChan, err := s.cli.KeepAlive(context.Background(), s.leaseId); err != nil {
		return err
	} else {
		s.keepAliveChan = respChan
		s.keepAliveFlag = true
	}
	go s.listenLeaseChan()
	return nil
}

func (s *ServiceRegister) listenLeaseChan() {
	for {
		select {
		case _, ok := <-s.keepAliveChan:
			if !ok {
				s.keepAliveFlag = false
				ELog.Errorf("ServiceRegister %v KeepAlive Error", s.key)
				go s.reconnect()
				return
			}
		}
	}
}

func (s *ServiceRegister) reconnect() {
	updTimer := time.NewTicker(30 * time.Second)
	defer updTimer.Stop()

	for {
		select {
		case <-updTimer.C:
			{
				err := s.connect()
				if err != nil {
					ELog.Errorf("ServiceRegister %v %v Reconnect Error=%v", s.key, s.val, err)
				} else {
					ELog.Infof("ServiceRegister %v %v Reconnect Success! ", s.key, s.val)
					return
				}
			}
		}
	}
}

func (s *ServiceRegister) CloseService() error {
	//撤销租约
	if _, err := s.cli.Revoke(context.Background(), s.leaseId); err != nil {
		return err
	}
	ELog.Infof("ServiceRegister CloseService Key=%v Value=%v Success!", s.key, s.val)
	return nil
}

var GServiceRegister *ServiceRegister

func init() {
	GServiceRegister = newServiceRegister()
}
