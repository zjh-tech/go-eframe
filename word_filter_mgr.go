package eframe

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strings"
	"unicode"
)

// 考虑字典树代替正则表达式实现
type WordFilterMgr struct {
	sensitiveWords      []string
	sensitiveWordRegexp *regexp.Regexp
}

func NewWordFilterMgr() *WordFilterMgr {
	return &WordFilterMgr{
		sensitiveWords:      make([]string, 0),
		sensitiveWordRegexp: nil,
	}
}

func (w *WordFilterMgr) Init(path string) error {
	file, err := os.OpenFile(path, os.O_RDONLY, 0)
	if err != nil {
		return err
	}
	defer file.Close()

	sensitiveWords := make([]string, 0)
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		content := scanner.Text()
		if content == "" {
			continue
		}
		content = strings.ReplaceAll(content, " ", "")
		flag := false
		for _, c := range content {
			if unicode.IsPunct(c) || unicode.IsSymbol(c) {
				flag = true
				break
			}
		}
		if flag {
			continue
		}
		sensitiveWords = append(sensitiveWords, content)
	}

	w.sensitiveWords = sensitiveWords
	regexpStr := strings.Join(w.sensitiveWords, "|")
	w.sensitiveWordRegexp = regexp.MustCompile(regexpStr)
	return nil
}

// 是否为敏感字
func (s *WordFilterMgr) IsSensitiveWords(context string) bool {
	checkStr := ""
	for _, c := range context {
		if unicode.Is(unicode.Han, c) {
			checkStr = fmt.Sprintf("%v%v", checkStr, string(c))
		}
	}
	for _, sensitiveWord := range s.sensitiveWords {
		if strings.Contains(checkStr, sensitiveWord) {
			return true
		}
	}
	return false
}

// 把敏感字改成*
func (w *WordFilterMgr) DoFilter(context string) ([]byte, bool) {
	hasSensitiveWords := false
	return w.sensitiveWordRegexp.ReplaceAllFunc([]byte(context), func(sensitiveWord []byte) []byte {
		replaceBytes := make([]byte, 0)
		sensitiveWordRunes := []rune(string(sensitiveWord))
		sensitiveWordLen := len(sensitiveWordRunes)
		for i := 0; i < sensitiveWordLen; i++ {
			replaceBytes = append(replaceBytes, byte('*'))
		}
		hasSensitiveWords = true
		return replaceBytes
	}), hasSensitiveWords
}

var GWordFilterMgr *WordFilterMgr
