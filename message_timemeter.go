package eframe

import (
	"fmt"
)

// 消息处理时间统计
type MessageTimeMeterUnit struct {
	Count     uint64
	TotalTime int64
	MaxTime   int64
}

type MessageTimeMeter struct {
	stampTicks map[uint64]*MessageTimeMeterUnit //ID - TotalTime
	id         uint64
	startTick  int64
}

func NewMessageTimeMeter() *MessageTimeMeter {
	return &MessageTimeMeter{
		stampTicks: make(map[uint64]*MessageTimeMeterUnit),
	}
}

func (m *MessageTimeMeter) Start(id uint64) {
	if id == 0 {
		return
	}

	m.id = id
	m.startTick = getMillsecond()
}

func (m *MessageTimeMeter) End() {
	if m.id == 0 {
		return
	}

	endTick := getMillsecond()
	onceTime := endTick - m.startTick
	if meter, ok := m.stampTicks[m.id]; ok {
		meter.TotalTime += onceTime
		meter.Count += 1
		if meter.MaxTime < onceTime {
			meter.MaxTime = onceTime
		}
	} else {
		unit := &MessageTimeMeterUnit{
			Count:     1,
			TotalTime: onceTime,
			MaxTime:   onceTime,
		}
		m.stampTicks[m.id] = unit
	}
}

func (m *MessageTimeMeter) Clear() {
	m.stampTicks = make(map[uint64]*MessageTimeMeterUnit)
	m.id = 0
	m.startTick = 0
}

func (m *MessageTimeMeter) CheckOut() {
	stampStr := "[Monitor] [MessageHandler]: "
	for id, unit := range m.stampTicks {
		stampStr = fmt.Sprintf("%v [%v-%v-%v]", stampStr, id, unit.TotalTime/int64(unit.Count), unit.MaxTime)
	}
	ELog.Info(stampStr)
}
