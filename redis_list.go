package eframe

import (
	"github.com/go-redis/redis"
)

type RedisList struct {
	cacheKey string
	redisCmd redis.Cmdable
}

func NewRedisList(cacheKey string, redisCmd redis.Cmdable) *RedisList {
	return &RedisList{
		cacheKey: cacheKey,
		redisCmd: redisCmd,
	}
}

// 添加
func (r *RedisList) AddData(value interface{}) error {
	_, err := r.redisCmd.LPush(r.cacheKey, value).Result()
	return err
}

// 删除某个节点
func (r *RedisList) DelData(value interface{}) error {
	_, err := r.redisCmd.LRem(r.cacheKey, 0, value).Result()
	return err
}

// 遍历
func (r *RedisList) RangeDatas() ([]string, error) {
	return r.redisCmd.LRange(r.cacheKey, 0, -1).Result()
}

// 长度
func (r *RedisList) Len() (int64, error) {
	return r.redisCmd.LLen(r.cacheKey).Result()
}

// 删除所有数据
func (r *RedisList) DelAllDatas() error {
	_, err := r.redisCmd.Del(r.cacheKey).Result()
	return err
}
