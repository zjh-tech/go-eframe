package eframe

import (
	"time"

	"gitee.com/zjh-tech/go-ebase/util"
)

type TimeResetMgr struct {
	hour int //几点重置
}

func NewTimeResetMgr() *TimeResetMgr {
	return &TimeResetMgr{}
}

func (t *TimeResetMgr) Init(hour int) {
	t.hour = hour
}

// 每天Hour点重置
func (t *TimeResetMgr) IsCanEveryDayReset(lastResetTime time.Time) bool {
	lastModuleResetTime := time.Date(lastResetTime.Year(), lastResetTime.Month(), lastResetTime.Day(), t.hour, 0, 0, 0, time.Local)
	now := time.Now()
	if lastResetTime.Before(lastModuleResetTime) {
		return now.After(lastModuleResetTime)
	}
	nextResetTime := lastModuleResetTime.AddDate(0, 0, 1)
	return now.After(nextResetTime)
}

func (t *TimeResetMgr) IsCanEveryDayResetBySec(lastResetSec int64) bool {
	lastResetTime := time.Unix(lastResetSec, 0)
	return t.IsCanEveryDayReset(lastResetTime)
}

func (t *TimeResetMgr) IsCanEveryDayResetByFormatString(lastResetTimeStr string) bool {
	lastResetTime, _ := util.GetTimeFromFormatString(lastResetTimeStr)
	return t.IsCanEveryDayReset(lastResetTime)
}

// 每星期一Hour点重置
func (t *TimeResetMgr) IsCanEveryWeekReset(lastResetTime time.Time) bool {
	lastModuleResetTime := t.GetFirstDateOfWeek(lastResetTime)
	now := time.Now()
	if lastResetTime.Before(lastModuleResetTime) {
		return now.After(lastModuleResetTime)
	}
	nextResetTime := lastModuleResetTime.AddDate(0, 0, 7)
	return now.After(nextResetTime)
}

func (t *TimeResetMgr) IsCanEveryWeekResetBySec(lastResetSec int64) bool {
	lastResetTime := time.Unix(lastResetSec, 0)
	return t.IsCanEveryWeekReset(lastResetTime)
}

func (t *TimeResetMgr) IsCanEveryWeekResetByFormatString(lastResetTimeStr string) bool {
	lastResetTime, _ := util.GetTimeFromFormatString(lastResetTimeStr)
	return t.IsCanEveryWeekReset(lastResetTime)
}

// 每月1号Hour点重置
func (t *TimeResetMgr) IsCanEveryMonthReset(lastResetTime time.Time) bool {
	lastModuleResetTime := time.Date(lastResetTime.Year(), lastResetTime.Month(), 1, t.hour, 0, 0, 0, time.Local)
	now := time.Now()
	if lastResetTime.Before(lastModuleResetTime) {
		return now.After(lastModuleResetTime)
	}
	nextResetTime := lastModuleResetTime.AddDate(0, 1, 0)
	return now.After(nextResetTime)
}

func (t *TimeResetMgr) IsCanEveryMonthResetBySec(lastResetSec int64) bool {
	lastResetTime := time.Unix(lastResetSec, 0)
	return t.IsCanEveryMonthReset(lastResetTime)
}

func (t *TimeResetMgr) IsCanEveryMonthResetByFormatString(lastResetTimeStr string) bool {
	lastResetTime, _ := util.GetTimeFromFormatString(lastResetTimeStr)
	return t.IsCanEveryMonthReset(lastResetTime)
}

// -------------------------------------------------------------------------------------------------------------------
// 本周周一Hour点
func (t *TimeResetMgr) GetFirstDateOfWeek(lastResetSec time.Time) time.Time {
	offset := int(time.Monday - lastResetSec.Weekday())
	if offset > 0 {
		offset = -6
	}
	return time.Date(lastResetSec.Year(), lastResetSec.Month(), lastResetSec.Day(), t.hour, 0, 0, 0, time.Local).AddDate(0, 0, offset)
}
