package eframe

import (
	"fmt"
	"time"

	"gitee.com/zjh-tech/go-ebase/convert"
	"gitee.com/zjh-tech/go-eredis"
)

// func Benchmark_Info(b *testing.B) {
// 	b.ReportAllocs()
// 	for i := 0; i < b.N; i++ {

// 	}
// }

func testTimeResetMgr() {
	//day
	timeResetMgr := NewTimeResetMgr()
	timeResetMgr.Init(5)

	lastResetSec := time.Date(2023, 4, 19, 4, 0, 0, 0, time.Local).UnixNano() / 1e9
	if timeResetMgr.IsCanEveryDayResetBySec(lastResetSec) {
		fmt.Println("Day Need Reset")
	} else {
		fmt.Println("Day Not Need Reset")
	}

	lastResetSec = time.Date(2023, 4, 19, 6, 0, 0, 0, time.Local).UnixNano() / 1e9
	if timeResetMgr.IsCanEveryDayResetBySec(lastResetSec) {
		fmt.Println("Day Need Reset")
	} else {
		fmt.Println("Day Not Need Reset")
	}

	//week
	if timeResetMgr.IsCanEveryWeekResetBySec(lastResetSec) {
		fmt.Println("Week Need Reset")
	} else {
		fmt.Println("Week Not Need Reset")
	}

	lastResetSec = time.Date(2023, 4, 17, 3, 0, 0, 0, time.Local).UnixNano() / 1e9
	if timeResetMgr.IsCanEveryWeekResetBySec(lastResetSec) {
		fmt.Println("Week Need Reset")
	} else {
		fmt.Println("Week Not Need Reset")
	}

	//month
	lastResetSec = time.Date(2023, 4, 1, 1, 0, 0, 0, time.Local).UnixNano() / 1e9
	if timeResetMgr.IsCanEveryMonthResetBySec(lastResetSec) {
		fmt.Println("Month Need Reset")
	} else {
		fmt.Println("Month Not Need Reset")
	}

	lastResetSec = time.Date(2023, 4, 1, 6, 0, 0, 0, time.Local).UnixNano() / 1e9
	if timeResetMgr.IsCanEveryMonthResetBySec(lastResetSec) {
		fmt.Println("Month Need Reset")
	} else {
		fmt.Println("Month Not Need Reset")
	}
}

func testRedisRank() {
	redisClient, err := eredis.ConnectRedis("192.168.31.230:7000", "")
	if err != nil {
		fmt.Print(err)
		return
	}

	now := time.Now()
	end := now.AddDate(0, 0, 1)
	redisRank := NewRedisRank("fight_rank", 40, redisClient, now, end)
	redisRank.CheckCapacity(true)

	for i := 0; i < 40; i++ {
		//redisRank.AddScore(100, fmt.Sprintf("Name_%v", i+1))
		redisRank.AddMixScore(100, fmt.Sprintf("Name_%v", i+1))
	}

	fmt.Println("Top")
	topMembers, _ := redisRank.GetTopScores(10)
	for _, member := range topMembers {
		fmt.Printf("Top Score=%v Member=%v\n", member.Score, member.Member)
	}

	fmt.Println("Low")

	lowMembers, _ := redisRank.GetLowScores(10)
	for _, member := range lowMembers {
		fmt.Printf("Low Score=%v Member=%v\n", member.Score, member.Member)
	}

	rank, err := redisRank.GetRankByMember("Name_40")
	if err == nil {
		fmt.Printf("Name_40 Rank=%v", rank)
	}

	redisRank.ClearAll()
}

func testRoleCache() {
	redisClient, err := eredis.ConnectRedis("192.168.31.230:7000", "")
	if err != nil {
		fmt.Print(err)
		return
	}

	roleCache := NewRedisHashMap("RoleCache", redisClient)

	for i := uint64(0); i < 100; i++ {
		roleCache.SetData(convert.ToString(i+1), fmt.Sprintf("RoleName_%v", i+1))
	}

	name, _ := roleCache.GetData(convert.ToString(4))
	fmt.Println(name)

	getnames, _ := roleCache.GetDatas([]string{convert.ToString(4), convert.ToString(6)})
	for _, getname := range getnames {
		fmt.Println(getname.(string))
	}

	roleCache.DelData(convert.ToString(5))
}

func init() {
	testFlag := 2
	switch testFlag {
	case 1:
		testTimeResetMgr()
	case 2:
		testRedisRank()
	case 3:
		testRoleCache()
	}
}
