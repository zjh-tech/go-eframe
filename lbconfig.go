package eframe

import "github.com/go-yaml/yaml"

type LbConfig struct {
	HttpSpec *HttpSpec `yaml:"http"`
}

func ReadLbConfig(content []byte) (*LbConfig, error) {
	cfg := &LbConfig{}
	if err := yaml.Unmarshal(content, &cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}

var GLbConfig *LbConfig
