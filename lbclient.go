package eframe

import (
	"encoding/json"
	"errors"
	"fmt"

	"gitee.com/zjh-tech/go-enet"
)

type LBClient struct {
	url        string
	handlerMap map[string]enet.HttpCbFunc
}

func NewLBClient() *LBClient {
	client := &LBClient{
		handlerMap: make(map[string]enet.HttpCbFunc),
	}
	return client
}

func (s *LBClient) Init(url string) bool {
	s.url = url
	// s.RegisterHandler("lb_register", OnHandlerLbRegisterRes)
	// s.RegisterHandler("select_min_service", OnHandlerSelectMinServiceRes)
	return true
}

func (s *LBClient) LBPostJson(router string, js interface{}) (interface{}, error) {
	fullUrl := "http://" + s.url + "/" + router

	content, postErr := enet.PostJson(fullUrl, js)
	if postErr != nil {
		ELog.Errorf("LBPost Error %v", postErr)
		return nil, postErr
	}
	return content, nil
	// httpEvt := enet.NewHttpEvent(s, router, content)
	// return httpEvt.ProcessMsg()
}

// func (s *LBClient) RegisterHandler(router string, handler enet.HttpCbFunc) {
// 	s.handlerMap[router] = handler
// }

// func (s *LBClient) OnHandler(router string, datas []byte) (interface{}, error) {
// 	handler, ok := s.handlerMap[router]
// 	if !ok {
// 		msg := fmt.Sprintf("LBClient OnHandler Not Find %v Error", router)
// 		ELog.Warnf(msg)
// 		return nil, errors.New(msg)
// 	}

// 	return handler(datas)
// }

// ------------------------------------SLB API-----------------------------------------------------

func (s *LBClient) SendLbRegisterReq(spec *LbRegisterReq) error {
	ret, err := s.LBPostJson("lb_register", spec)
	if err != nil {
		return err
	}
	datas := ret.([]byte)
	res := &LbRegisterRes{}
	unmarshalErr := json.Unmarshal(datas, &res)
	if unmarshalErr != nil {
		msg := fmt.Sprintf("LbRegisterRes json.Unmarshal Error %v", unmarshalErr)
		ELog.Error(msg)
		return errors.New(msg)
	}
	return nil
}

func (s *LBClient) GetMinService(etcdPrefix string, serviceType uint32) (uint64, string, error) {
	req := &SelectMinServiceReq{
		EtcdPrefix:  etcdPrefix,
		ServiceType: serviceType,
	}
	ret, err := s.LBPostJson("select_min_service", req)
	if err != nil {
		return 0, "", err
	}
	datas := ret.([]byte)
	res := &SelectMinServiceRes{}
	unmarshalErr := json.Unmarshal(datas, res)
	if unmarshalErr != nil {
		msg := fmt.Sprintf("SelectMinServiceRes json.Unmarshal Error %v", unmarshalErr)
		ELog.Error(msg)
		return 0, "", errors.New(msg)
	}

	if res.Code == 0 && res.ServiceID != 0 && res.ServiceType != 0 {
		return res.ServiceID, res.Addr, nil
	}
	return 0, "", errors.New("GetMinService Unknow Error")
}

var GLBClient *LBClient
