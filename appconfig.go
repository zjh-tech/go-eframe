package eframe

import (
	"io/ioutil"

	"github.com/go-yaml/yaml"
)

type EtcdConnSpec struct {
	Addr string `yaml:"addr"`
}

type AppConfig struct {
	ServerId     uint64          `yaml:"serverid"`
	ServerType   string          `yaml:"servertype"`
	EtcdPrefix   string          `yaml:"etcdprefix"`
	EtcdAddrList []*EtcdConnSpec `yaml:"etcd_addrlist"`
}

func NewAppConfig() *AppConfig {
	return &AppConfig{
		EtcdAddrList: make([]*EtcdConnSpec, 0),
	}
}

func (a *AppConfig) GetEctdAddrs() []string {
	addrs := make([]string, 0)
	for _, info := range a.EtcdAddrList {
		addrs = append(addrs, info.Addr)
	}
	return addrs
}

func ReadAppConfig(path string) (*AppConfig, error) {
	content, readErr := ioutil.ReadFile(path)
	if readErr != nil {
		return nil, readErr
	}
	cfg := &AppConfig{}
	if err := yaml.Unmarshal(content, &cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}

var GAppConfig *AppConfig
