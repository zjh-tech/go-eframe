package eframe

import (
	"runtime/debug"
	"sync/atomic"

	"gitee.com/zjh-tech/go-elog"
)

type ExecAsyncFunc func([]interface{}) (*AsyncItemResult, error)
type ExecAsyncCbFunc func(paras []interface{}, ret *AsyncItemResult, err error)

type AsyncItemResult struct {
	paras []interface{}
}

func (a *AsyncItemResult) GetParas() []interface{} {
	return a.paras
}

func NewAsyncItemResult(v ...interface{}) *AsyncItemResult {
	itemRet := &AsyncItemResult{
		paras: make([]interface{}, 0),
	}

	itemRet.paras = append(itemRet.paras, v...)
	return itemRet
}

type AsyncItem struct {
	execFunc   ExecAsyncFunc
	execCbFunc ExecAsyncCbFunc
	paras      []interface{}
	ret        *AsyncItemResult
	err        error
}

func NewAsyncItem(execFunc ExecAsyncFunc, execCbFunc ExecAsyncCbFunc, paras []interface{}) *AsyncItem {
	return &AsyncItem{
		execFunc:   execFunc,
		execCbFunc: execCbFunc,
		paras:      paras,
	}
}

func (a *AsyncItem) ExecFunc() {
	a.ret, a.err = a.execFunc(a.paras)
}

func (a *AsyncItem) HasCbFunc() bool {
	return a.execCbFunc != nil
}

func (a *AsyncItem) ExecCbFunc() {
	a.execCbFunc(a.paras, a.ret, a.err)
}

const (
	MAX_ASYNC_COUNT        = 10000000
	OrderFreeState  uint32 = 0
	OrderBusyState  uint32 = 1
)

type AsyncModule struct {
	cbQueue    chan *AsyncItem
	orderQueue chan *AsyncItem
	orderState uint32
}

func NewAsyncModule(count int) *AsyncModule {
	if count == 0 {
		count = MAX_ASYNC_COUNT
	}
	return &AsyncModule{
		cbQueue:    make(chan *AsyncItem, count),
		orderQueue: make(chan *AsyncItem, MAX_ASYNC_COUNT),
	}
}

// 并发执行
func (a *AsyncModule) ConcurrentDoOpt(execFunc ExecAsyncFunc, execCbFunc ExecAsyncCbFunc, attach []interface{}) {
	go func(execFunc ExecAsyncFunc, execCbFunc ExecAsyncCbFunc, attach []interface{}) {
		defer func() {
			if err := recover(); err != nil {
				elog.Errorf("Run Error=%v Stack=%v", err, string(debug.Stack()))
			}
		}()
		item := NewAsyncItem(execFunc, execCbFunc, attach)
		item.ExecFunc()
		if item.HasCbFunc() {
			a.cbQueue <- item
		}
	}(execFunc, execCbFunc, attach)
}

// 顺序执行:一些要保证顺序的逻辑业务
func (a *AsyncModule) OrderDoOpt(execFunc ExecAsyncFunc, execCbFunc ExecAsyncCbFunc, attach []interface{}) {
	item := NewAsyncItem(execFunc, execCbFunc, attach)
	a.orderQueue <- item

	if atomic.CompareAndSwapUint32(&a.orderState, OrderFreeState, OrderBusyState) {
		go a.doOrderGoroutine()
	}
}

func (a *AsyncModule) doOrderGoroutine() {
	defer func() {
		atomic.CompareAndSwapUint32(&a.orderState, OrderBusyState, OrderFreeState)
		if err := recover(); err != nil {
			elog.Errorf("Run Error=%v Stack=%v", err, string(debug.Stack()))
		}
	}()

	for {
		select {
		case item, ok := <-a.orderQueue:
			if !ok {
				return
			}
			item.ExecFunc()
			if item.HasCbFunc() {
				a.cbQueue <- item
			}
		default:
			return
		}
	}
}

func (a *AsyncModule) Run() {
	for {
		select {
		case item, ok := <-a.GetTriggerChannel():
			if ok {
				a.ExecTriggerLogic(item)
			}
		default:
			return
		}
	}
}

func (a *AsyncModule) GetTriggerChannel() chan *AsyncItem {
	return a.cbQueue
}

func (a *AsyncModule) ExecTriggerLogic(item *AsyncItem) {
	item.ExecCbFunc()
}

var GAsyncModule *AsyncModule
