package eframe

import (
	"encoding/json"
	"runtime/debug"

	"gitee.com/zjh-tech/go-elog"
	"gitee.com/zjh-tech/go-enet"
	"google.golang.org/protobuf/proto"
)

type MsServerFunc func(c2sMsTranferMsg *C2SMsTranferMsg, sess *enet.CSSession, m *MsServer) bool

type MsServerOnConnectCbFunc func(sess *enet.CSSession, m *MsServer)

type MsServerOnDisconnectCbFunc func(sess *enet.CSSession, m *MsServer)

type MsServer struct {
	dealer        *enet.IDDealer
	tranferDealer *enet.IDDealer

	onConnectCb    MsServerOnConnectCbFunc
	onDisconnectCb MsServerOnDisconnectCbFunc
}

func NewMsServer(connectCb MsServerOnConnectCbFunc, disConnectCb MsServerOnDisconnectCbFunc) *MsServer {
	server := &MsServer{
		dealer:         enet.NewIDDealer(),
		tranferDealer:  enet.NewIDDealer(),
		onConnectCb:    connectCb,
		onDisconnectCb: disConnectCb,
	}
	server.Init()
	return server
}

func (m *MsServer) Init() bool {
	m.dealer.RegisterHandler(uint32(C2SMsVerifyMsgId), MsServerFunc(OnHandlerC2SMsVerifyMsg))
	m.dealer.RegisterHandler(uint32(C2SMsPingMsgId), MsServerFunc(OnHandlerC2SMsPingMsg))
	m.dealer.RegisterHandler(uint32(C2SMsTranferMsgId), MsServerFunc(OnHandlerC2SMsTranferMsg))
	return true
}

func (m *MsServer) RegisterHandler(msgId uint32, handlerFunc MsServerFunc) bool {
	m.dealer.RegisterHandler(msgId, handlerFunc)
	return true
}

func (m *MsServer) RegisterTranferHandler(msgId uint32, dealer interface{}) bool {
	m.tranferDealer.RegisterHandler(msgId, dealer)
	return true
}

func (m *MsServer) GetTranferDealer() *enet.IDDealer {
	return m.tranferDealer
}

func (m *MsServer) OnHandler(msgId uint32, datas []byte, sess *enet.CSSession) {
	defer func() {
		if err := recover(); err != nil {
			elog.Errorf("MsServer onHandler MsgID = %v Error=%v Stack=%v", msgId, err, string(debug.Stack()))
		}
	}()

	dealer := m.dealer.FindHandler(msgId)
	if dealer == nil {
		return
	}
	msg := &C2SMsTranferMsg{
		MsgId: msgId,
		Datas: datas,
	}
	dealer.(MsServerFunc)(msg, sess, m)
}

func (m *MsServer) OnConnect(sess *enet.CSSession) {
	sessId := sess.GetSessID()
	sess.SetHeartBeatMaxTime(int64(MS_HEARTBEAT_MAX_TIME))
	elog.Infof("MsServer OnConnect SessID=%v ", sessId)
}

func (m *MsServer) OnDisconnect(sess *enet.CSSession) {
	sessId := sess.GetSessID()
	elog.Infof("MsServer OnDisconnect SessID=%v ", sessId)

	if m.onDisconnectCb != nil {
		m.onDisconnectCb(sess, m)
	}
}

func (m *MsServer) OnVerifySessTimeOutError(sess *enet.CSSession) {
	sessId := sess.GetSessID()
	elog.Infof("MsServer OnVerifySessTimeOutError SessID=%v ", sessId)
}

func (m *MsServer) OnBeatHeartError(sess *enet.CSSession) {
	sessId := sess.GetSessID()
	elog.Infof("MsServer OnBeatHeartError SessID=%v ", sessId)
}

func (m *MsServer) SendProtoMsg(sess *enet.CSSession, msgId uint32, msg proto.Message) bool {
	return sess.SendProtoMsg(msgId, msg)
}

func (m *MsServer) SendS2CMsTranferMsg(sess *enet.CSSession, cbId uint64, msgId uint32, msg proto.Message) bool {
	tranferMsg := &S2CMsTranferMsg{
		MsgId: msgId,
		CbId:  cbId,
	}

	datas, pbMarshalErr := proto.Marshal(msg)
	if pbMarshalErr != nil {
		ELog.Errorf("[MsServer] Marshal Err=%v ", pbMarshalErr)
		return false
	}
	tranferMsg.Datas = datas

	jsonDatas, jsonMarshalErr := json.Marshal(tranferMsg)
	if jsonMarshalErr != nil {
		ELog.Errorf("[MsServer]  Json Marshal Err %v ", jsonMarshalErr)
		return false
	}
	return sess.SendMsg(S2CMsTranferMsgId, jsonDatas)
}

func OnHandlerC2SMsVerifyMsg(c2sMsTranferMsg *C2SMsTranferMsg, sess *enet.CSSession, m *MsServer) bool {
	req := &C2SMsVerifyMsg{}
	unmarshalErr := json.Unmarshal(c2sMsTranferMsg.Datas, req)
	if unmarshalErr != nil {
		return false
	}

	res := &S2CMsVerifyMsg{}
	if req.Token == GServer.GetToken() {
		elog.Infof("[MsServer] SessionID=%v Verify Ok", sess.GetSessID())
		res.ErrorCode = MSG_SUCCESS_ERRORCODE
		sess.SetVerifySessOk()
		if m.onConnectCb != nil {
			m.onConnectCb(sess, m)
		}
	} else {
		elog.Errorf("[MsServer] SessionID=%v Verify Fail", sess.GetSessID())
		res.ErrorCode = MSG_SUCCESS_ERRORCODE
	}

	sess.SendJsonMsg(uint32(S2CMsVerifyMsgId), res)
	return true
}

func OnHandlerC2SMsTranferMsg(c2sMsTranferMsg *C2SMsTranferMsg, sess *enet.CSSession, m *MsServer) bool {
	req := &C2SMsTranferMsg{}
	unmarshalErr := json.Unmarshal(c2sMsTranferMsg.Datas, req)
	if unmarshalErr != nil {
		return false
	}

	tranferDealer := m.tranferDealer.FindHandler(req.MsgId)
	if tranferDealer == nil {
		ELog.Errorf("[MsServer] OnHandlerC2SMsTranferMsg Not Find MsgId=%v", req.MsgId)
		return false
	}
	tranferDealer.(MsServerFunc)(req, sess, m)
	return true
}

func OnHandlerC2SMsPingMsg(c2sMsTranferMsg *C2SMsTranferMsg, sess *enet.CSSession, m *MsServer) bool {
	res := &S2CMsPongMsg{}
	sess.SendJsonMsg(uint32(S2CMsPongMsgId), res)
	ELog.Debugf("SessionID=%v Send Pong", sess.GetSessID())
	return true
}
