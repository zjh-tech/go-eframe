package eframe

import (
	"context"
	"fmt"

	clientv3 "go.etcd.io/etcd/client/v3"
)

type ConfigCenterClient struct {
	cli *clientv3.Client
}

func newConfigCenterClient() *ConfigCenterClient {
	return &ConfigCenterClient{}
}

func (c *ConfigCenterClient) Init(cli *clientv3.Client) bool {
	if cli == nil {
		return false
	}

	c.cli = cli
	return true
}

func (c *ConfigCenterClient) SetConfig(key string, value string) error {
	_, err := c.cli.Put(context.Background(), key, value)
	return err
}

func (c *ConfigCenterClient) GetConfigDatas(key string) ([]byte, error) {
	ELog.Infof("ConfigCenterClient Key=%v", key)
	resp, err := c.cli.Get(context.Background(), key)
	if err != nil {
		return nil, err
	}

	for _, kv := range resp.Kvs {
		return kv.Value, nil
	}
	return nil, fmt.Errorf("[ConfigCenterClient] GetDatas Key=%v Error", key)
}

var GConfigCenterClient *ConfigCenterClient

func init() {
	GConfigCenterClient = newConfigCenterClient()
}
