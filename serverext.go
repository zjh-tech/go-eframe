package eframe

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"runtime"
	"time"

	"gitee.com/zjh-tech/go-ebase/util"
	"gitee.com/zjh-tech/go-edb"
	"gitee.com/zjh-tech/go-elog"
	"gitee.com/zjh-tech/go-enet"
	"gitee.com/zjh-tech/go-eredis"
	"gitee.com/zjh-tech/go-etimer"
	"github.com/google/gops/agent"
)

func (s *Server) InitAppLogic(logPrefix string) {
	s.mainCtx = context.Background()
	s.mainCtxWithCancel, s.mainCancelFunc = context.WithCancel(s.mainCtx)

	runtime.GOMAXPROCS(runtime.NumCPU())

	configPath := "."
	if s.IsDebugMode() {
		//F5 调试模式
		configPath = os.Args[1]
	}
	s.configPath = configPath

	s.terminate = false
	s.logPrefix = logPrefix
	s.logger = elog.NewLogger(APP_LOG_PATH, fmt.Sprintf("%s.app", logPrefix))
	s.logger.Init(s.mainCtxWithCancel, &s.wg)
	ELog.Init(s.logger, APP_LOG_LEVEL)

	s.tomorrowZeroTime = util.GetTomorrowZeroTime()
}

func (s *Server) InitServerLogic() error {
	//Log
	s.logger.UnInit()
	fileDirPrefix := s.logger.GetFileDirPrefix()
	if fileDirPrefix != "" {
		os.RemoveAll(fileDirPrefix)
	}

	if s.GetServerId() != 0 {
		s.logger = elog.NewLogger(GServerCfg.LogInfo.Path, fmt.Sprintf("%v%v", s.logPrefix, s.GetServerId()))
	} else {
		s.logger = elog.NewLogger(GServerCfg.LogInfo.Path, s.logPrefix)
	}

	s.logger.Init(s.mainCtxWithCancel, &s.wg)
	ELog.Init(s.logger, GEngineLogCfg.LogInfo.FrameLevel)

	s.initModulesLog()
	ELog.Info("Server Log System Init Success")
	s.stampModulesVersion()

	localIp, ipErr := util.GetLocalIp()
	if ipErr != nil {
		return fmt.Errorf("Server GetLocalIp Error=%v", ipErr)
	}
	s.ip = localIp

	s.timerRegister = etimer.NewTimerRegister(etimer.GTimerMgr)

	//Signal
	GSignalDealer.SetQuitDealer(s)
	go GSignalDealer.StartSignalGoroutine()

	//Gops
	gopsErr := agent.Listen(agent.Options{
		Addr:            "",
		ConfigDir:       "",
		ShutdownCleanup: true,
	})

	if gopsErr != nil {
		return fmt.Errorf("Server Gops Error=%v", gopsErr)
	}

	enet.GNet.Init(s.mainCtx)

	GServer = s
	return nil
}

func (s *Server) InitConfigCenter() error {
	if GAppConfig.EtcdAddrList == nil {
		return fmt.Errorf("Server EctdInfo Is Nil")
	}

	ectdAddrs := GAppConfig.GetEctdAddrs()
	if len(ectdAddrs) == 0 {
		return fmt.Errorf("Server EctdAddrs Len = 0")
	}

	ectdCli, ectdConnErr := ConnectEtcd(ectdAddrs, ETCD_CONNECT_TIMEOUT)
	if ectdConnErr != nil {
		return fmt.Errorf("Server EctdAddrs=%v Error=%v", ectdAddrs, ectdConnErr)
	}
	for _, etcdAddr := range ectdAddrs {
		ELog.Infof("Connect Etcd Addr=%v Success", etcdAddr)
	}
	GEtcdClient = ectdCli
	if !GConfigCenterClient.Init(ectdCli) {
		return fmt.Errorf("Server GConfigCenterClient Init Error")
	}
	return nil
}

func (s *Server) InitModuleByServerCfg() error {
	//Redis Cluster/Client
	if GServerCfg.IsOpenRedis() {
		redisAddrs := GServerCfg.GetRedisAddrs()
		if len(redisAddrs) == 0 {
			return fmt.Errorf("redis addrs len is empty")
		}

		if GServerCfg.IsOpenRedisCluster() {
			if redisCluster, err := eredis.ConnectRedisCluster(redisAddrs, GServerCfg.RedisInfo.Password); err != nil {
				return fmt.Errorf("redis addrs=%v connect cluster error=%v", redisAddrs, err)
			} else {
				s.RedisCmd = redisCluster
				ELog.Infof("Redis Addrs=%v Connect Cluster Success", redisAddrs)
			}
		} else {
			if redisClient, err := eredis.ConnectRedis(redisAddrs[0], GServerCfg.RedisInfo.Password); err != nil {
				return fmt.Errorf("redis addrs=%v connect error", redisAddrs[0])
			} else {
				s.RedisCmd = redisClient
				ELog.Infof("Redis Addrs=%v Connect Success", redisAddrs[0])
			}
		}
	}

	//Db
	if GServerCfg.IsOpenDB() {
		edb.GDBModule = edb.NewDBModule()
		if GServerCfg.DBInfo != nil && GServerCfg.DBInfo.TableMaxCount != 0 {
			if err := edb.GDBModule.Init(s.mainCtx, &s.wg, GServerCfg.DBInfo.TableMaxCount, GServerCfg.DBInfo.DBInfoList); err != nil {
				return fmt.Errorf("%v", err)
			}
		}
	}

	//Tcp
	enet.GSSSessionMgr = enet.NewSSSessionMgr()
	enet.GSSSessionMgr.Init(s.GetToken())
	if len(GServerCfg.ServerInfo.Inner) != 0 {
		if !enet.GSSSessionMgr.SSServerListen(GServerCfg.ServerInfo.Inner, enet.BigSendChannelSize, "SSSession") {
			return fmt.Errorf("Server Listen Inter=%v", GServerCfg.ServerInfo.Inner)
		}
	}

	return nil
}

func (s *Server) InitServiceDiscovery() error {
	if !GServiceDiscovery.Init(s.mainCtxWithCancel, GEtcdClient, AddServiceCbFunc, DelServiceCbFunc, UpdServiceCbFunc) {
		return fmt.Errorf("Server GServiceDiscovery Init Error")
	}

	connectKey := "public/connect.yaml"
	connectCfgValue, connectCfgErr := GConfigCenterClient.GetConfigDatas(connectKey)
	if connectCfgErr != nil {
		return fmt.Errorf("Server Connect.yaml GetConfigDatas Error=%v", connectCfgErr)
	}
	connectCfg, readConnectErr := ReadConnectCfg(connectCfgValue)
	if readConnectErr != nil {
		return fmt.Errorf("Server Connect.yaml ReadConnectCfg Error=%v", readConnectErr)
	} else {
		GConnectCfg = connectCfg
	}

	localKey := fmt.Sprintf("%v/%v", GServerCfg.ServerInfo.EtcdPrefix, GServerCfg.ServerInfo.ServerTypeStr)
	GServerCfg.WatchServerList = GConnectCfg.GetWatchServerList(localKey)

	watchServerList := GServerCfg.WatchServerList
	for _, watchServer := range watchServerList {
		watchServicePrefix := fmt.Sprintf("/service/%v/", watchServer) //必须加上"/"结尾,防止监听relation后,relationgateserver,relationserver都被监听到
		watchErr := GServiceDiscovery.WatchService(s.mainCtx, watchServicePrefix)
		if watchErr != nil {
			return watchErr
		}
	}

	if !GServiceRegister.Init(GEtcdClient) {
		return fmt.Errorf("Server GServiceRegister Init Error")
	}

	return s.StartRegisterService()
}

func (s *Server) StartRegisterService() error {
	etcdServiceInfo := &EctdServiceSpec{
		ServerId:      GServerCfg.ServerInfo.ServerId,
		ServerType:    s.serverType,
		ServerTypeStr: s.GetServerTypeStr(),
		Token:         GServerCfg.ServerInfo.Token,
		Inner:         GServerCfg.ServerInfo.Inner,
		Outer:         GServerCfg.ServerInfo.Outer,
	}

	serviceJson, marshalError := json.Marshal(etcdServiceInfo)
	if marshalError != nil {
		return fmt.Errorf("Server EtcdServiceInfo Json.Marshal Error=%v", marshalError)
	}
	serviceKey := fmt.Sprintf("/service/%v/%v/%v", s.GetEtcdPrefix(), s.GetServerTypeStr(), etcdServiceInfo.ServerId)
	GServiceRegister.StartService(serviceKey, string(serviceJson), SERVICE_REGISTER_ETCD_LEASE_SEC)
	return nil
}

func (s *Server) InitServerLb() error {
	if !s.lbInitFlag {
		return nil
	}

	s.lbSendChan = make(chan struct{}, 100)

	lbKey := "public/lb.yaml"
	lbCfgValue, lbCfgErr := GConfigCenterClient.GetConfigDatas(lbKey)
	if lbCfgErr != nil {
		return fmt.Errorf("Server Lb.yaml GetConfigDatas Error=%v", lbCfgErr)
	}
	lbCfg, readLbErr := ReadLbConfig(lbCfgValue)
	if readLbErr != nil {
		return fmt.Errorf("Server Lb.yaml ReadLbConfig Error=%v", readLbErr)
	}

	GLbConfig = lbCfg
	GLBClient = NewLBClient()
	if !GLBClient.Init(GLbConfig.HttpSpec.Url) {
		return fmt.Errorf("lb init error")
	}
	if s.lbFlag {
		s.LbNotify()
		s.StartLbGoroutine()
	}
	ELog.Infof("LBClient Url=%v Init Success", GLbConfig.HttpSpec.Url)
	return nil
}

func (s *Server) initModulesLog() {
	//IOC依赖注入
	etimer.ELog.Init(s.logger, GEngineLogCfg.LogInfo.TimerLevel)
	edb.ELog.Init(s.logger, GEngineLogCfg.LogInfo.DbLevel)
	enet.ELog.Init(s.logger, GEngineLogCfg.LogInfo.NetLevel)
	eredis.ELog.Init(s.logger, GEngineLogCfg.LogInfo.RedisLevel)
}

func (s *Server) stampModulesVersion() {
	modules := []IVersion{elog.GLogVersion, edb.GDBVersion, eredis.GRedisVersion, etimer.GTimerVersion, enet.GNetVersion}
	for _, module := range modules {
		ELog.Info(module.GetVersion())
	}
}

func (s *Server) UpdateZeorFrame() {
	now := time.Now()
	if now.After(s.tomorrowZeroTime) {
		s.tomorrowZeroTime = s.tomorrowZeroTime.AddDate(0, 0, 1)
		if s.zeroLogicFunc != nil {
			s.zeroLogicFunc()
		}
	}
}

func (s *Server) SetZeroLogicFunc(zeroLogicFunc ZeroLogicFuncType) {
	s.zeroLogicFunc = zeroLogicFunc
}

func (s *Server) StartIDMaker() error {
	if s.GetServerId() == 0 {
		return fmt.Errorf("ServerID = 0 IDMaker Error")
	}

	idMaker, idErr := NewIdMaker(int64(s.GetServerId()), true)
	if idErr != nil {
		return fmt.Errorf("Error=%v", idErr)
	}
	GIdMaker = idMaker
	return nil
}
