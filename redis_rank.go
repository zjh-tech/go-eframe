package eframe

import (
	"fmt"
	"time"

	"gitee.com/zjh-tech/go-ebase/util"
	"gitee.com/zjh-tech/go-elog"
	"github.com/go-redis/redis"
)

// 默认从高到低排序
// 做一些简单的排行

type RedisRank struct {
	sortKey  string
	capacity int64
	redisCmd redis.Cmdable

	startTime      time.Time // 活动开始时间
	startTimestamp int64

	endTime      time.Time // 活动结束时间戳
	endTimestamp int64
}

func NewRedisRank(sortKey string, capacity int64, redisCmd redis.Cmdable, startTime time.Time, endTime time.Time) *RedisRank {
	return &RedisRank{
		sortKey:        sortKey,
		capacity:       capacity,
		redisCmd:       redisCmd,
		startTime:      startTime,
		startTimestamp: startTime.UnixNano() / 1e6,
		endTime:        endTime,
		endTimestamp:   endTime.UnixNano() / 1e6,
	}
}

// 加入/更新
func (r *RedisRank) AddScore(score int64, member interface{}) error {
	if _, err := r.redisCmd.ZAdd(r.sortKey, redis.Z{Score: float64(score), Member: member}).Result(); err != nil {
		return err
	}
	r.CheckCapacity(false)
	return nil
}

// 加入/更新: score + 时间戳 一起排序
// score限制区间: [0-4194304]
func (r *RedisRank) AddMixScore(score int64, member interface{}) error {
	now := util.GetMillsecond()
	if now < r.startTimestamp {
		return fmt.Errorf("%v Is Not Start", r.sortKey)
	}

	if now > r.endTimestamp {
		return fmt.Errorf("%v Is End", r.sortKey)
	}

	mixScore := int64(0)
	var err error

	if mixScore, err = r.GetMixScore(score, r.endTimestamp-now); err != nil {
		return err
	}
	if _, err := r.redisCmd.ZAdd(r.sortKey, redis.Z{Score: float64(mixScore), Member: member}).Result(); err != nil {
		return err
	}

	r.CheckCapacity(false)
	return nil
}

// 取分数最高的n个
func (r *RedisRank) GetTopScores(topNum int64) ([]redis.Z, error) {
	ret, err := r.redisCmd.ZRevRangeWithScores(r.sortKey, 0, topNum).Result()
	if err != nil {
		return nil, fmt.Errorf("%v GetTopScores Error=%v", r.sortKey, err)
	}
	return ret, nil
}

// 取分数最低的n个
func (r *RedisRank) GetLowScores(lowNum int64) ([]redis.Z, error) {
	ret, err := r.redisCmd.ZRangeWithScores(r.sortKey, 0, lowNum).Result()
	if err != nil {
		return nil, fmt.Errorf("%v GetLowScores Error=%v", r.sortKey, err)
	}
	return ret, nil
}

// 获取自己的当前排名
func (r *RedisRank) GetRankByMember(member string) (int64, error) {
	index, err := r.redisCmd.ZRank(r.sortKey, member).Result()
	if err != nil {
		return 0, err
	}
	return index, nil
}

// 清空所有榜单
func (r *RedisRank) ClearAll() error {
	_, err := r.redisCmd.Del(r.sortKey).Result()
	if err != nil {
		return err
	}
	return nil
}

// 检查是否超过容量,超过容量就删除
func (r *RedisRank) CheckCapacity(initFlag bool) {
	count, err2 := r.redisCmd.ZCard(r.sortKey).Result()
	if err2 != nil {
		elog.Errorf("SortKey=%v ZPopMin Error=%v", r.sortKey, err2)
	}

	if count > r.capacity {
		needDelCount := int64(1)
		if initFlag {
			needDelCount = count - r.capacity
		}
		_, err3 := r.redisCmd.ZPopMin(r.sortKey, needDelCount).Result()
		if err3 != nil {
			elog.Errorf("SortKey=%v ZPopMin Error=%v", r.sortKey, err3)
		}
	}
}

// MixScore = 第1位为正数 + Score(2^22) + 41位的时间截
// MixScore = score + score2 / SCORE2MAX([0, 1048575]==> 2^20)
func (r *RedisRank) GetMixScore(score int64, tick int64) (int64, error) {
	if score >= 4194304 {
		return 0, fmt.Errorf("%v GetMixScore Exceed Max Value", r.sortKey)
	}

	mixScore := score << 41
	mixScore = mixScore | tick
	return mixScore, nil
}

func (r *RedisRank) GetScoreFromMixScore(mixScore int64) int64 {
	return mixScore >> 41
}
