package eframe

import (
	"io/ioutil"

	"github.com/go-yaml/yaml"
)

type EngineLogCfg struct {
	LogInfo EngineLogInfo `yaml:"enginelog"`
}

func NewEngineLogCfg() *EngineLogCfg {
	return &EngineLogCfg{}
}

func ReadEngineLogCfg(content []byte) (*EngineLogCfg, error) {
	cfg := NewEngineLogCfg()
	if err := yaml.Unmarshal(content, &cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}

func ReadEngineLogCfgByPath(path string) (*EngineLogCfg, error) {
	content, readErr := ioutil.ReadFile(path)
	if readErr != nil {
		return nil, readErr
	}

	cfg := NewEngineLogCfg()
	if err := yaml.Unmarshal(content, &cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}

var GEngineLogCfg *EngineLogCfg
