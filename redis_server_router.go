package eframe

import (
	"fmt"

	"gitee.com/zjh-tech/go-ebase/convert"
	"github.com/go-redis/redis"
)

func SetServerRouterToRedis(rounter string, id uint64, serverId uint64) error {
	key := GetServerRouterKey(rounter, id)
	if err := GServer.RedisCmd.Set(key, serverId, -1).Err(); err != nil {
		return err
	}
	return nil
}

func GetRouterServerFromRedis(rounter string, id uint64) (uint64, bool, error) {
	key := GetServerRouterKey(rounter, id)
	val, err := GServer.RedisCmd.Get(key).Result()
	if err == redis.Nil {
		return 0, false, nil
	}

	serverId, convertErr := convert.StrToUint64(val)
	if convertErr != nil {
		return 0, true, convertErr
	}
	return serverId, true, nil
}

func DelServerRouterToRedis(rounter string, id uint64) error {
	key := GetServerRouterKey(rounter, id)
	if err := GServer.RedisCmd.Del(key).Err(); err != nil {
		return err
	}
	return nil
}

// ----------------------------------------------------------------------
func GetServerRouterKey(rounter string, id uint64) string {
	return fmt.Sprintf("%s%v", rounter, id)
}
