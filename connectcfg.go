package eframe

import (
	"strings"

	"github.com/go-yaml/yaml"
)

type ConnectSpec struct {
	Connect string `yaml:"connect"`
}

type ConnectCfg struct {
	ConnectList []*ConnectSpec `yaml:"connectlist"`
}

func (c *ConnectCfg) GetWatchServerList(localKey string) []string {
	ret := make([]string, 0)

	if c.ConnectList != nil {
		for _, connectInfo := range c.ConnectList {
			keys := strings.Split(connectInfo.Connect, "-")
			if len(keys) == 2 {
				if keys[0] == localKey {
					watchServers := strings.Split(keys[1], "|")
					ret = watchServers
					break
				}
			}
		}
	}
	return ret
}

func ReadConnectCfg(content []byte) (*ConnectCfg, error) {
	cfg := &ConnectCfg{}
	if err := yaml.Unmarshal(content, &cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}

var GConnectCfg *ConnectCfg
