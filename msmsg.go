package eframe

const (
	MsEngineMinMsgId = 101

	C2SMsVerifyMsgId = 102
	S2CMsVerifyMsgId = 103

	C2SMsTranferMsgId = 104
	S2CMsTranferMsgId = 105

	C2SMsPingMsgId = 106
	S2CMsPongMsgId = 107

	MsEngineMaxMsgId = 200
)

type C2SMsVerifyMsg struct {
	Token string
}

type S2CMsVerifyMsg struct {
	ErrorCode uint32
}

type C2SMsTranferMsg struct {
	MsgId       uint32
	Datas       []byte
	CbId        uint64
	AttachDatas []string
}

type S2CMsTranferMsg struct {
	MsgId uint32
	Datas []byte
	CbId  uint64
}

type C2SMsPingMsg struct {
}

type S2CMsPongMsg struct {
}
