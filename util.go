package eframe

import (
	"strings"

	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/reflect/protoreflect"
)

func ProtoJsonMarshal(m protoreflect.ProtoMessage) ([]byte, error) {
	jsonBytes, err := protojson.Marshal(m)
	if err != nil {
		return nil, err
	}
	jsonStr := string(jsonBytes)
	jsonStr = strings.ReplaceAll(jsonStr, ", \"", ",\"") //去掉空格
	return []byte(jsonStr), nil
}
