package eframe

import (
	"encoding/json"
	"sync"

	"gitee.com/zjh-tech/go-ebase/util"
	"gitee.com/zjh-tech/go-enet"
	"gitee.com/zjh-tech/go-etimer"
	"google.golang.org/protobuf/proto"
)

type MsClientFunc func(datas []byte, sess *enet.CSSession, m *MsClient) bool

type MsCbFunc func(cbArgs []interface{}, datas []byte) bool

type MsCbArgType = []interface{}

type MsCbItem struct {
	MsgId  uint32
	Msg    proto.Message
	CbFunc MsCbFunc
	CbAgrs MsCbArgType
	Tick   int64
}

type MsWithOutCbFunc func(datas []byte, m *MsClient) bool

func NewMsCbItem(msgId uint32, msg proto.Message, cbFunc MsCbFunc, cbArgs MsCbArgType) *MsCbItem {
	return &MsCbItem{
		MsgId:  msgId,
		Msg:    msg,
		CbFunc: cbFunc,
		CbAgrs: cbArgs,
		Tick:   util.GetMillsecond() + int64(MS_CLIENT_CALLBACK_TIMEOUT_TIMER_DELAY),
	}
}

type MsClientOnConnectCbFunc func(sess *enet.CSSession)

type MsClientOnDisconnectCbFunc func(sess *enet.CSSession)

const (
	MS_CLIENT_CALLBACK_TIMEOUT_TIMER_ID uint32 = 1
	MS_CLIENT_RECONNECT_TIME_ID         uint32 = 2
	MS_CLIENT_HEARTBEAT_TIME_ID         uint32 = 3
)

const (
	MS_CLIENT_CALLBACK_TIMEOUT_TIMER_DELAY uint64 = 1000 * 60 * 5
	MS_CLIENT_RECONNECT_TIME_DELAY         uint64 = 1000 * 10
	MS_CLIENT_HEARTBEAT_TIME_DELAY         uint64 = 1000 * 60 * 1
)

const (
	MS_HEARTBEAT_MAX_TIME uint64 = 1000 * 60 * 4
)

type MsClient struct {
	timerRegister etimer.ITimerRegister

	safeFlag bool
	mutex    sync.Mutex

	desc string

	addr   string
	sessId uint64

	sessMgr *enet.CSSessionMgr
	sess    *enet.CSSession

	token string

	cbNextId uint64 //回调
	cbMap    map[uint64]*MsCbItem

	dealer        *enet.IDDealer //不回调
	tranferDealer *enet.IDDealer //不回调

	onConnectCb    MsClientOnConnectCbFunc
	onDisconnectCb MsClientOnDisconnectCbFunc
}

func NewMsClient() *MsClient {
	client := &MsClient{
		timerRegister: etimer.NewTimerRegister(etimer.GTimerMgr),
		cbNextId:      0,
		cbMap:         make(map[uint64]*MsCbItem),
		sessMgr:       enet.NewCSSessionMgr(),
		dealer:        enet.NewIDDealer(),
		tranferDealer: enet.NewIDDealer(),
	}

	return client
}

func (m *MsClient) Init(desc string, addr string, token string, connectCb MsClientOnConnectCbFunc, disConnectCb MsClientOnDisconnectCbFunc, safeFlag bool) bool {
	m.desc = desc
	m.addr = addr
	m.token = token
	m.onConnectCb = connectCb
	m.onDisconnectCb = disConnectCb
	m.safeFlag = safeFlag

	m.RegisterHandler(uint32(S2CMsVerifyMsgId), MsClientFunc(OnHandlerS2CMsVerifyMsg))
	m.RegisterHandler(uint32(S2CMsPongMsgId), MsClientFunc(OnHandlerS2CMsPongMsg))
	m.RegisterHandler(uint32(S2CMsTranferMsgId), MsClientFunc(OnHandlerS2CMsTranferMsg))

	m.timerRegister.AddRepeatTimer(MS_CLIENT_CALLBACK_TIMEOUT_TIMER_ID, MS_CLIENT_CALLBACK_TIMEOUT_TIMER_DELAY, func(v ...interface{}) {
		for cbId, cbItem := range m.cbMap {
			if cbItem.Tick < util.GetMillsecond() {
				ELog.Warnf("[MsClient] %v TimeOut MsgId=%+v", m.desc, cbItem.MsgId)
				delete(m.cbMap, cbId)
			}
		}
	}, []interface{}{})

	m.timerRegister.AddRepeatTimer(MS_CLIENT_RECONNECT_TIME_ID, MS_CLIENT_RECONNECT_TIME_DELAY, func(v ...interface{}) {
		if m.sessId == 0 || (!m.sessMgr.IsInConnectCache(m.sessId) && !m.sessMgr.IsExistSession(m.sessId)) {
			m.reset()
			m.sessId = m.sessMgr.Connect(m.addr, m, nil, enet.BigSendChannelSize, nil)
			ELog.Infof("[MsClient] %v Addr=%v Reconnect SessionId=%v", m.desc, m.addr, m.sessId)
		}
	}, []interface{}{})

	m.timerRegister.AddRepeatTimer(MS_CLIENT_HEARTBEAT_TIME_ID, MS_CLIENT_HEARTBEAT_TIME_DELAY, func(v ...interface{}) {
		if m.sess != nil {
			req := &C2SMsPingMsg{}
			m.sess.SendJsonMsg(uint32(C2SMsPingMsgId), req)
			ELog.Debugf("SessionID=%v Send Ping", m.sess.GetSessID())
		}
	}, []interface{}{})

	return true
}

func (m *MsClient) Unit() {
	m.timerRegister.KillAllTimer()
}

func (m *MsClient) RegisterHandler(msgId uint32, handlerFunc MsClientFunc) bool {
	m.dealer.RegisterHandler(msgId, handlerFunc)
	return true
}

func (m *MsClient) RegisterTranferHandler(msgId uint32, dealer interface{}) bool {
	m.tranferDealer.RegisterHandler(msgId, dealer)
	return true
}

func (m *MsClient) GetTranferDealer() *enet.IDDealer {
	return m.tranferDealer
}

func (m *MsClient) SendTranferMsg(msgId uint32, msg proto.Message, attachDatas []string, cbFunc MsCbFunc, cbArgs MsCbArgType) bool {
	if m.sess == nil {
		ELog.Errorf("[MsClient] %v Not Find CSSession", m.desc)
		return false
	}

	if !m.sess.IsVerifySessOk() {
		ELog.Errorf("[MsClient] %v Not VerifyOk", m.desc)
		return false
	}

	datas, pbMarshalErr := proto.Marshal(msg)
	if pbMarshalErr != nil {
		ELog.Errorf("[MsClient] %v MsgId=%v Marshal Err=%v ", m.desc, msgId, pbMarshalErr)
		return false
	}

	tranferMsg := &C2SMsTranferMsg{
		MsgId:       msgId,
		Datas:       datas,
		AttachDatas: attachDatas,
	}

	if cbFunc != nil {
		cbItem := NewMsCbItem(msgId, msg, cbFunc, cbArgs)
		cbId := m.nextId()
		m.cbMap[cbId] = cbItem
		tranferMsg.CbId = cbId
	}

	jsonDatas, jsonMarshalErr := json.Marshal(tranferMsg)
	if jsonMarshalErr != nil {
		ELog.Errorf("[MsClient] %v MsgId=%v Json Marshal Err %v ", m.desc, msgId, jsonMarshalErr)
		return false
	}

	return m.sess.SendMsg(uint32(C2SMsTranferMsgId), jsonDatas)
}

func (m *MsClient) SendTranferBytes(msgId uint32, datas []byte, attachDatas []string) bool {
	if m.sess == nil {
		ELog.Errorf("[MsClient] %v Not Find CSSession", m.desc)
		return false
	}

	if !m.sess.IsVerifySessOk() {
		ELog.Errorf("[MsClient] %v Not VerifyOk", m.desc)
		return false
	}

	tranferMsg := &C2SMsTranferMsg{
		MsgId:       msgId,
		Datas:       datas,
		AttachDatas: attachDatas,
	}

	jsonDatas, jsonMarshalErr := json.Marshal(tranferMsg)
	if jsonMarshalErr != nil {
		ELog.Errorf("[MsClient] %v MsgId=%v Json Marshal Err %v ", m.desc, msgId, jsonMarshalErr)
		return false
	}

	return m.sess.SendMsg(uint32(C2SMsTranferMsgId), jsonDatas)
}

// --------------------------------------------------------------------
func (m *MsClient) SendProtoMsg(msgId uint32, msg proto.Message) bool {
	if m.sess == nil {
		ELog.Errorf("[MsClient] %v Not Find CSSession", m.desc)
		return false
	}

	return m.sess.SendProtoMsg(msgId, msg)
}

func (m *MsClient) SendMsg(msgId uint32, datas []byte) bool {
	if m.sess == nil {
		ELog.Errorf("[MsClient] %v Not Find CSSession", m.desc)
		return false
	}
	return m.sess.SendMsg(msgId, datas)
}

// ---------------------------------------------------------------------
func (m *MsClient) Update() {
	m.sessMgr.Update()
}

func (m *MsClient) reset() {
	m.sessId = 0
	m.sess = nil
}

func (m *MsClient) nextId() uint64 {
	if m.safeFlag {
		m.mutex.Lock()
		defer m.mutex.Unlock()
	}

	m.cbNextId++
	return m.cbNextId
}

func (m *MsClient) OnHandler(msgID uint32, datas []byte, sess *enet.CSSession) {
	defer func() {
		if err := recover(); err != nil {
			ELog.Errorf("[MsClient] %v OnHandler MsgID=%v Error=%v", m.desc, msgID, err)
		}
	}()

	dealer := m.dealer.FindHandler(msgID)
	if dealer == nil {
		ELog.Errorf("[MsClient] %v MsgHandler Can Not Find MsgID=%v", m.desc, msgID)
		return
	}
	dealer.(MsClientFunc)(datas, sess, m)
}

func (m *MsClient) OnConnect(sess *enet.CSSession) {
	ELog.Infof("[MsClient] %v SessId=%v OnConnect", m.desc, sess.GetSessID())
	m.sess = sess
	m.sess.SetVerifySessOk()

	req := &C2SMsVerifyMsg{
		Token: m.token,
	}
	sess.SendJsonMsg(uint32(C2SMsVerifyMsgId), req)

	sess.SetHeartBeatMaxTime(int64(MS_HEARTBEAT_MAX_TIME))
}

func (m *MsClient) OnDisconnect(sess *enet.CSSession) {
	ELog.Infof("[MsClient] %v SessId=%v OnDisconnect", m.desc, sess.GetSessID())
	m.reset()

	if m.onDisconnectCb != nil {
		m.onDisconnectCb(sess)
	}
}

func (m *MsClient) OnBeatHeartError(sess *enet.CSSession) {
	ELog.Errorf("[MsClient] %v SessId=%v OnBeatHeartError", m.desc, sess.GetSessID())
}

func (m *MsClient) OnVerifySessTimeOutError(sess *enet.CSSession) {
	ELog.Errorf("[MsClient] %v SessId=%v OnVerifySessTimeOutError", m.desc, sess.GetSessID())
}

func OnHandlerS2CMsVerifyMsg(datas []byte, sess *enet.CSSession, m *MsClient) bool {
	verifyResMsg := &S2CMsVerifyMsg{}
	unmarshalErr := json.Unmarshal(datas, verifyResMsg)
	if unmarshalErr != nil {
		return false
	}

	if verifyResMsg.ErrorCode == MSG_SUCCESS_ERRORCODE {
		if m.onConnectCb != nil {
			m.onConnectCb(sess)
		}
	}
	return true
}

func OnHandlerS2CMsPongMsg(datas []byte, sess *enet.CSSession, m *MsClient) bool {
	sess.SetHeartBeatNormal()
	return true
}

func OnHandlerS2CMsTranferMsg(datas []byte, sess *enet.CSSession, m *MsClient) bool {
	tranferMsg := &S2CMsTranferMsg{}
	unmarshalErr := json.Unmarshal(datas, tranferMsg)
	if unmarshalErr != nil {
		return false
	}

	cbItem, ok := m.cbMap[tranferMsg.CbId]
	if ok {
		delete(m.cbMap, tranferMsg.CbId)
		if cbItem.CbFunc != nil {
			cbItem.CbFunc(cbItem.CbAgrs, tranferMsg.Datas)
		}
		return true
	} else {
		tranferDealer := m.tranferDealer.FindHandler(tranferMsg.MsgId)
		if tranferDealer == nil {
			ELog.Errorf("[MsClient] %v OnHandlerS2CMsTranferMsg MsgId=%v Not Find", m.desc, tranferMsg.MsgId)
			return false
		}
		tranferDealer.(MsWithOutCbFunc)(tranferMsg.Datas, m)
	}

	return true
}
